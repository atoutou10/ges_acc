import { Moment } from 'moment';
import { IAccord } from 'app/shared/model/accord.model';

export interface IActivite {
  id?: number;
  photo?: string;
  titre?: string;
  soustitre?: string;
  objectif?: string;
  description?: string;
  datedeb?: Moment;
  datefin?: Moment;
  status?: boolean;
  accord?: IAccord;
}

export class Activite implements IActivite {
  constructor(
    public id?: number,
    public photo?: string,
    public titre?: string,
    public soustitre?: string,
    public objectif?: string,
    public description?: string,
    public datedeb?: Moment,
    public datefin?: Moment,
    public status?: boolean,
    public accord?: IAccord
  ) {
    this.status = this.status || false;
  }
}
