import { Moment } from 'moment';
import { IComite } from 'app/shared/model/comite.model';
import { IActivite } from 'app/shared/model/activite.model';
import { IAvenant } from 'app/shared/model/avenant.model';
import { IPartenaire } from 'app/shared/model/partenaire.model';

export interface IAccord {
  id?: number;
  nom?: string;
  titre?: string;
  domaine?: string;
  description?: string;
  but?: string;
  dateSignature?: Moment;
  dateExpiration?: Moment;
  status?: boolean;
  comite?: IComite;
  activites?: IActivite[];
  avenant?: IAvenant;
  partenaire?: IPartenaire;
}

export class Accord implements IAccord {
  constructor(
    public id?: number,
    public nom?: string,
    public titre?: string,
    public domaine?: string,
    public description?: string,
    public but?: string,
    public dateSignature?: Moment,
    public dateExpiration?: Moment,
    public status?: boolean,
    public comite?: IComite,
    public activites?: IActivite[],
    public avenant?: IAvenant,
    public partenaire?: IPartenaire
  ) {
    this.status = this.status || false;
  }
}
