import { IAccord } from 'app/shared/model/accord.model';
import { IDemandeAccord } from 'app/shared/model/demande-accord.model';
import { ITypePartenaire } from 'app/shared/model/type-partenaire.model';

export interface IPartenaire {
  id?: number;
  nom?: string;
  domaine?: string;
  pays?: string;
  accords?: IAccord[];
  demandes?: IDemandeAccord[];
  typepartenaire?: ITypePartenaire;
}

export class Partenaire implements IPartenaire {
  constructor(
    public id?: number,
    public nom?: string,
    public domaine?: string,
    public pays?: string,
    public accords?: IAccord[],
    public demandes?: IDemandeAccord[],
    public typepartenaire?: ITypePartenaire
  ) {}
}
