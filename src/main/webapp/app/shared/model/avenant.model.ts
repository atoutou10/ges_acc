import { Moment } from 'moment';

export interface IAvenant {
  id?: number;
  libelle?: string;
  document?: string;
  but?: string;
  description?: string;
  dateSignature?: Moment;
  dateExpiration?: Moment;
  status?: boolean;
}

export class Avenant implements IAvenant {
  constructor(
    public id?: number,
    public libelle?: string,
    public document?: string,
    public but?: string,
    public description?: string,
    public dateSignature?: Moment,
    public dateExpiration?: Moment,
    public status?: boolean
  ) {
    this.status = this.status || false;
  }
}
