import { IComite } from 'app/shared/model/comite.model';

export interface IParticipant {
  id?: number;
  nom?: string;
  cin?: string;
  prenom?: string;
  email?: string;
  telephone?: string;
  entreprise?: string;
  poste?: string;
  comite?: IComite;
}

export class Participant implements IParticipant {
  constructor(
    public id?: number,
    public nom?: string,
    public cin?: string,
    public prenom?: string,
    public email?: string,
    public telephone?: string,
    public entreprise?: string,
    public poste?: string,
    public comite?: IComite
  ) {}
}
