import { Moment } from 'moment';
import { IPartenaire } from 'app/shared/model/partenaire.model';

export interface IDemandeAccord {
  id?: number;
  num?: string;
  titre?: string;
  type?: string;
  description?: string;
  texteAccord?: string;
  dateDemande?: Moment;
  validation?: boolean;
  partenaire?: IPartenaire;
}

export class DemandeAccord implements IDemandeAccord {
  constructor(
    public id?: number,
    public num?: string,
    public titre?: string,
    public type?: string,
    public description?: string,
    public texteAccord?: string,
    public dateDemande?: Moment,
    public validation?: boolean,
    public partenaire?: IPartenaire
  ) {
    this.validation = this.validation || false;
  }
}
