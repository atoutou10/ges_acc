export interface ITypePartenaire {
  id?: number;
  libelle?: string;
}

export class TypePartenaire implements ITypePartenaire {
  constructor(public id?: number, public libelle?: string) {}
}
