import { IParticipant } from 'app/shared/model/participant.model';
import { IAccord } from 'app/shared/model/accord.model';

export interface IComite {
  id?: number;
  role?: string;
  nom?: string;
  participants?: IParticipant[];
  accord?: IAccord;
}

export class Comite implements IComite {
  constructor(
    public id?: number,
    public role?: string,
    public nom?: string,
    public participants?: IParticipant[],
    public accord?: IAccord
  ) {}
}
