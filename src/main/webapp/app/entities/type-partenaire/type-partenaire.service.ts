import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITypePartenaire } from 'app/shared/model/type-partenaire.model';

type EntityResponseType = HttpResponse<ITypePartenaire>;
type EntityArrayResponseType = HttpResponse<ITypePartenaire[]>;

@Injectable({ providedIn: 'root' })
export class TypePartenaireService {
  public resourceUrl = SERVER_API_URL + 'api/type-partenaires';

  constructor(protected http: HttpClient) {}

  create(typePartenaire: ITypePartenaire): Observable<EntityResponseType> {
    return this.http.post<ITypePartenaire>(this.resourceUrl, typePartenaire, { observe: 'response' });
  }

  update(typePartenaire: ITypePartenaire): Observable<EntityResponseType> {
    return this.http.put<ITypePartenaire>(this.resourceUrl, typePartenaire, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITypePartenaire>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITypePartenaire[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
