import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITypePartenaire, TypePartenaire } from 'app/shared/model/type-partenaire.model';
import { TypePartenaireService } from './type-partenaire.service';

@Component({
  selector: 'jhi-type-partenaire-update',
  templateUrl: './type-partenaire-update.component.html',
})
export class TypePartenaireUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    libelle: [],
  });

  constructor(protected typePartenaireService: TypePartenaireService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ typePartenaire }) => {
      this.updateForm(typePartenaire);
    });
  }

  updateForm(typePartenaire: ITypePartenaire): void {
    this.editForm.patchValue({
      id: typePartenaire.id,
      libelle: typePartenaire.libelle,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const typePartenaire = this.createFromForm();
    if (typePartenaire.id !== undefined) {
      this.subscribeToSaveResponse(this.typePartenaireService.update(typePartenaire));
    } else {
      this.subscribeToSaveResponse(this.typePartenaireService.create(typePartenaire));
    }
  }

  private createFromForm(): ITypePartenaire {
    return {
      ...new TypePartenaire(),
      id: this.editForm.get(['id'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITypePartenaire>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
