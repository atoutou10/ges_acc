import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GesAccSharedModule } from 'app/shared/shared.module';
import { TypePartenaireComponent } from './type-partenaire.component';
import { TypePartenaireDetailComponent } from './type-partenaire-detail.component';
import { TypePartenaireUpdateComponent } from './type-partenaire-update.component';
import { TypePartenaireDeleteDialogComponent } from './type-partenaire-delete-dialog.component';
import { typePartenaireRoute } from './type-partenaire.route';

@NgModule({
  imports: [GesAccSharedModule, RouterModule.forChild(typePartenaireRoute)],
  declarations: [
    TypePartenaireComponent,
    TypePartenaireDetailComponent,
    TypePartenaireUpdateComponent,
    TypePartenaireDeleteDialogComponent,
  ],
  entryComponents: [TypePartenaireDeleteDialogComponent],
})
export class GesAccTypePartenaireModule {}
