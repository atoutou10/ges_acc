import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITypePartenaire, TypePartenaire } from 'app/shared/model/type-partenaire.model';
import { TypePartenaireService } from './type-partenaire.service';
import { TypePartenaireComponent } from './type-partenaire.component';
import { TypePartenaireDetailComponent } from './type-partenaire-detail.component';
import { TypePartenaireUpdateComponent } from './type-partenaire-update.component';

@Injectable({ providedIn: 'root' })
export class TypePartenaireResolve implements Resolve<ITypePartenaire> {
  constructor(private service: TypePartenaireService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITypePartenaire> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((typePartenaire: HttpResponse<TypePartenaire>) => {
          if (typePartenaire.body) {
            return of(typePartenaire.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TypePartenaire());
  }
}

export const typePartenaireRoute: Routes = [
  {
    path: '',
    component: TypePartenaireComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.typePartenaire.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TypePartenaireDetailComponent,
    resolve: {
      typePartenaire: TypePartenaireResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.typePartenaire.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TypePartenaireUpdateComponent,
    resolve: {
      typePartenaire: TypePartenaireResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.typePartenaire.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TypePartenaireUpdateComponent,
    resolve: {
      typePartenaire: TypePartenaireResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.typePartenaire.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
