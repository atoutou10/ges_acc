import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITypePartenaire } from 'app/shared/model/type-partenaire.model';
import { TypePartenaireService } from './type-partenaire.service';

@Component({
  templateUrl: './type-partenaire-delete-dialog.component.html',
})
export class TypePartenaireDeleteDialogComponent {
  typePartenaire?: ITypePartenaire;

  constructor(
    protected typePartenaireService: TypePartenaireService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.typePartenaireService.delete(id).subscribe(() => {
      this.eventManager.broadcast('typePartenaireListModification');
      this.activeModal.close();
    });
  }
}
