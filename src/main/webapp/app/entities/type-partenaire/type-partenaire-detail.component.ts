import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITypePartenaire } from 'app/shared/model/type-partenaire.model';

@Component({
  selector: 'jhi-type-partenaire-detail',
  templateUrl: './type-partenaire-detail.component.html',
})
export class TypePartenaireDetailComponent implements OnInit {
  typePartenaire: ITypePartenaire | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ typePartenaire }) => (this.typePartenaire = typePartenaire));
  }

  previousState(): void {
    window.history.back();
  }
}
