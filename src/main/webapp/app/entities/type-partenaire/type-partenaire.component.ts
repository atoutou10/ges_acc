import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITypePartenaire } from 'app/shared/model/type-partenaire.model';
import { TypePartenaireService } from './type-partenaire.service';
import { TypePartenaireDeleteDialogComponent } from './type-partenaire-delete-dialog.component';

@Component({
  selector: 'jhi-type-partenaire',
  templateUrl: './type-partenaire.component.html',
})
export class TypePartenaireComponent implements OnInit, OnDestroy {
  typePartenaires?: ITypePartenaire[];
  eventSubscriber?: Subscription;

  constructor(
    protected typePartenaireService: TypePartenaireService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.typePartenaireService.query().subscribe((res: HttpResponse<ITypePartenaire[]>) => (this.typePartenaires = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTypePartenaires();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITypePartenaire): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTypePartenaires(): void {
    this.eventSubscriber = this.eventManager.subscribe('typePartenaireListModification', () => this.loadAll());
  }

  delete(typePartenaire: ITypePartenaire): void {
    const modalRef = this.modalService.open(TypePartenaireDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.typePartenaire = typePartenaire;
  }
}
