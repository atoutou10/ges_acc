import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { IAccord, Accord } from 'app/shared/model/accord.model';
import { AccordService } from './accord.service';
import { IComite } from 'app/shared/model/comite.model';
import { ComiteService } from 'app/entities/comite/comite.service';
import { IAvenant } from 'app/shared/model/avenant.model';
import { AvenantService } from 'app/entities/avenant/avenant.service';
import { IPartenaire } from 'app/shared/model/partenaire.model';
import { PartenaireService } from 'app/entities/partenaire/partenaire.service';

type SelectableEntity = IComite | IAvenant | IPartenaire;

@Component({
  selector: 'jhi-accord-update',
  templateUrl: './accord-update.component.html',
})
export class AccordUpdateComponent implements OnInit {
  isSaving = false;

  comites: IComite[] = [];

  avenants: IAvenant[] = [];

  partenaires: IPartenaire[] = [];
  dateSignatureDp: any;
  dateExpirationDp: any;

  editForm = this.fb.group({
    id: [],
    nom: [],
    titre: [],
    domaine: [],
    description: [],
    but: [],
    dateSignature: [],
    dateExpiration: [],
    status: [],
    comite: [],
    avenant: [],
    partenaire: [],
  });

  constructor(
    protected accordService: AccordService,
    protected comiteService: ComiteService,
    protected avenantService: AvenantService,
    protected partenaireService: PartenaireService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ accord }) => {
      this.updateForm(accord);

      this.comiteService
        .query({ filter: 'accord-is-null' })
        .pipe(
          map((res: HttpResponse<IComite[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IComite[]) => {
          if (!accord.comite || !accord.comite.id) {
            this.comites = resBody;
          } else {
            this.comiteService
              .find(accord.comite.id)
              .pipe(
                map((subRes: HttpResponse<IComite>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IComite[]) => {
                this.comites = concatRes;
              });
          }
        });

      this.avenantService
        .query()
        .pipe(
          map((res: HttpResponse<IAvenant[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IAvenant[]) => (this.avenants = resBody));

      this.partenaireService
        .query()
        .pipe(
          map((res: HttpResponse<IPartenaire[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IPartenaire[]) => (this.partenaires = resBody));
    });
  }

  updateForm(accord: IAccord): void {
    this.editForm.patchValue({
      id: accord.id,
      nom: accord.nom,
      titre: accord.titre,
      domaine: accord.domaine,
      description: accord.description,
      but: accord.but,
      dateSignature: accord.dateSignature,
      dateExpiration: accord.dateExpiration,
      status: accord.status,
      comite: accord.comite,
      avenant: accord.avenant,
      partenaire: accord.partenaire,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const accord = this.createFromForm();
    if (accord.id !== undefined) {
      this.subscribeToSaveResponse(this.accordService.update(accord));
    } else {
      this.subscribeToSaveResponse(this.accordService.create(accord));
    }
  }

  private createFromForm(): IAccord {
    return {
      ...new Accord(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      titre: this.editForm.get(['titre'])!.value,
      domaine: this.editForm.get(['domaine'])!.value,
      description: this.editForm.get(['description'])!.value,
      but: this.editForm.get(['but'])!.value,
      dateSignature: this.editForm.get(['dateSignature'])!.value,
      dateExpiration: this.editForm.get(['dateExpiration'])!.value,
      status: this.editForm.get(['status'])!.value,
      comite: this.editForm.get(['comite'])!.value,
      avenant: this.editForm.get(['avenant'])!.value,
      partenaire: this.editForm.get(['partenaire'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAccord>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
