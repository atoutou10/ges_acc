import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IActivite, Activite } from 'app/shared/model/activite.model';
import { ActiviteService } from './activite.service';
import { IAccord } from 'app/shared/model/accord.model';
import { AccordService } from 'app/entities/accord/accord.service';

@Component({
  selector: 'jhi-activite-update',
  templateUrl: './activite-update.component.html',
})
export class ActiviteUpdateComponent implements OnInit {
  isSaving = false;
  accords: IAccord[] = [];
  datedebDp: any;
  datefinDp: any;

  editForm = this.fb.group({
    id: [],
    photo: [],
    titre: [],
    soustitre: [],
    objectif: [],
    description: [],
    datedeb: [],
    datefin: [],
    status: [],
    accord: [],
  });

  constructor(
    protected activiteService: ActiviteService,
    protected accordService: AccordService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ activite }) => {
      this.updateForm(activite);

      this.accordService.query().subscribe((res: HttpResponse<IAccord[]>) => (this.accords = res.body || []));
    });
  }

  updateForm(activite: IActivite): void {
    this.editForm.patchValue({
      id: activite.id,
      photo: activite.photo,
      titre: activite.titre,
      soustitre: activite.soustitre,
      objectif: activite.objectif,
      description: activite.description,
      datedeb: activite.datedeb,
      datefin: activite.datefin,
      status: activite.status,
      accord: activite.accord,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const activite = this.createFromForm();
    if (activite.id !== undefined) {
      this.subscribeToSaveResponse(this.activiteService.update(activite));
    } else {
      this.subscribeToSaveResponse(this.activiteService.create(activite));
    }
  }

  private createFromForm(): IActivite {
    return {
      ...new Activite(),
      id: this.editForm.get(['id'])!.value,
      photo: this.editForm.get(['photo'])!.value,
      titre: this.editForm.get(['titre'])!.value,
      soustitre: this.editForm.get(['soustitre'])!.value,
      objectif: this.editForm.get(['objectif'])!.value,
      description: this.editForm.get(['description'])!.value,
      datedeb: this.editForm.get(['datedeb'])!.value,
      datefin: this.editForm.get(['datefin'])!.value,
      status: this.editForm.get(['status'])!.value,
      accord: this.editForm.get(['accord'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IActivite>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IAccord): any {
    return item.id;
  }
}
