import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IActivite } from 'app/shared/model/activite.model';

type EntityResponseType = HttpResponse<IActivite>;
type EntityArrayResponseType = HttpResponse<IActivite[]>;

@Injectable({ providedIn: 'root' })
export class ActiviteService {
  public resourceUrl = SERVER_API_URL + 'api/activites';

  constructor(protected http: HttpClient) {}

  create(activite: IActivite): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(activite);
    return this.http
      .post<IActivite>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(activite: IActivite): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(activite);
    return this.http
      .put<IActivite>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IActivite>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IActivite[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(activite: IActivite): IActivite {
    const copy: IActivite = Object.assign({}, activite, {
      datedeb: activite.datedeb && activite.datedeb.isValid() ? activite.datedeb.format(DATE_FORMAT) : undefined,
      datefin: activite.datefin && activite.datefin.isValid() ? activite.datefin.format(DATE_FORMAT) : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.datedeb = res.body.datedeb ? moment(res.body.datedeb) : undefined;
      res.body.datefin = res.body.datefin ? moment(res.body.datefin) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((activite: IActivite) => {
        activite.datedeb = activite.datedeb ? moment(activite.datedeb) : undefined;
        activite.datefin = activite.datefin ? moment(activite.datefin) : undefined;
      });
    }
    return res;
  }
}
