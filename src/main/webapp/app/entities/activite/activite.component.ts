import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IActivite } from 'app/shared/model/activite.model';
import { ActiviteService } from './activite.service';
import { ActiviteDeleteDialogComponent } from './activite-delete-dialog.component';

@Component({
  selector: 'jhi-activite',
  templateUrl: './activite.component.html',
})
export class ActiviteComponent implements OnInit, OnDestroy {
  activites?: IActivite[];
  eventSubscriber?: Subscription;

  constructor(protected activiteService: ActiviteService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.activiteService.query().subscribe((res: HttpResponse<IActivite[]>) => (this.activites = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInActivites();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IActivite): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInActivites(): void {
    this.eventSubscriber = this.eventManager.subscribe('activiteListModification', () => this.loadAll());
  }

  delete(activite: IActivite): void {
    const modalRef = this.modalService.open(ActiviteDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.activite = activite;
  }
}
