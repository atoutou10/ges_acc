import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IPartenaire, Partenaire } from 'app/shared/model/partenaire.model';
import { PartenaireService } from './partenaire.service';
import { ITypePartenaire } from 'app/shared/model/type-partenaire.model';
import { TypePartenaireService } from 'app/entities/type-partenaire/type-partenaire.service';

@Component({
  selector: 'jhi-partenaire-update',
  templateUrl: './partenaire-update.component.html',
})
export class PartenaireUpdateComponent implements OnInit {
  isSaving = false;

  typepartenaires: ITypePartenaire[] = [];

  editForm = this.fb.group({
    id: [],
    nom: [],
    domaine: [],
    pays: [],
    typepartenaire: [],
  });

  constructor(
    protected partenaireService: PartenaireService,
    protected typePartenaireService: TypePartenaireService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ partenaire }) => {
      this.updateForm(partenaire);

      this.typePartenaireService
        .query()
        .pipe(
          map((res: HttpResponse<ITypePartenaire[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: ITypePartenaire[]) => (this.typepartenaires = resBody));
    });
  }

  updateForm(partenaire: IPartenaire): void {
    this.editForm.patchValue({
      id: partenaire.id,
      nom: partenaire.nom,
      domaine: partenaire.domaine,
      pays: partenaire.pays,
      typepartenaire: partenaire.typepartenaire,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const partenaire = this.createFromForm();
    if (partenaire.id !== undefined) {
      this.subscribeToSaveResponse(this.partenaireService.update(partenaire));
    } else {
      this.subscribeToSaveResponse(this.partenaireService.create(partenaire));
    }
  }

  private createFromForm(): IPartenaire {
    return {
      ...new Partenaire(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      domaine: this.editForm.get(['domaine'])!.value,
      pays: this.editForm.get(['pays'])!.value,
      typepartenaire: this.editForm.get(['typepartenaire'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPartenaire>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ITypePartenaire): any {
    return item.id;
  }
}
