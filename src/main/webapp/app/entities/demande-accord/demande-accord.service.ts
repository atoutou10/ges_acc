import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDemandeAccord } from 'app/shared/model/demande-accord.model';

type EntityResponseType = HttpResponse<IDemandeAccord>;
type EntityArrayResponseType = HttpResponse<IDemandeAccord[]>;

@Injectable({ providedIn: 'root' })
export class DemandeAccordService {
  public resourceUrl = SERVER_API_URL + 'api/demande-accords';

  constructor(protected http: HttpClient) {}

  create(demandeAccord: IDemandeAccord): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(demandeAccord);
    return this.http
      .post<IDemandeAccord>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(demandeAccord: IDemandeAccord): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(demandeAccord);
    return this.http
      .put<IDemandeAccord>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDemandeAccord>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDemandeAccord[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(demandeAccord: IDemandeAccord): IDemandeAccord {
    const copy: IDemandeAccord = Object.assign({}, demandeAccord, {
      dateDemande:
        demandeAccord.dateDemande && demandeAccord.dateDemande.isValid() ? demandeAccord.dateDemande.format(DATE_FORMAT) : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateDemande = res.body.dateDemande ? moment(res.body.dateDemande) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((demandeAccord: IDemandeAccord) => {
        demandeAccord.dateDemande = demandeAccord.dateDemande ? moment(demandeAccord.dateDemande) : undefined;
      });
    }
    return res;
  }
}
