import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { IDemandeAccord, DemandeAccord } from 'app/shared/model/demande-accord.model';
import { DemandeAccordService } from './demande-accord.service';
import { IPartenaire } from 'app/shared/model/partenaire.model';
import { PartenaireService } from 'app/entities/partenaire/partenaire.service';

@Component({
  selector: 'jhi-demande-accord-update',
  templateUrl: './demande-accord-update.component.html',
})
export class DemandeAccordUpdateComponent implements OnInit {
  isSaving = false;

  partenaires: IPartenaire[] = [];
  dateDemandeDp: any;

  editForm = this.fb.group({
    id: [],
    num: [],
    titre: [],
    type: [],
    description: [],
    texteAccord: [],
    dateDemande: [],
    validation: [],
    partenaire: [],
  });

  constructor(
    protected demandeAccordService: DemandeAccordService,
    protected partenaireService: PartenaireService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ demandeAccord }) => {
      this.updateForm(demandeAccord);

      this.partenaireService
        .query()
        .pipe(
          map((res: HttpResponse<IPartenaire[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IPartenaire[]) => (this.partenaires = resBody));
    });
  }

  updateForm(demandeAccord: IDemandeAccord): void {
    this.editForm.patchValue({
      id: demandeAccord.id,
      num: demandeAccord.num,
      titre: demandeAccord.titre,
      type: demandeAccord.type,
      description: demandeAccord.description,
      texteAccord: demandeAccord.texteAccord,
      dateDemande: demandeAccord.dateDemande,
      validation: demandeAccord.validation,
      partenaire: demandeAccord.partenaire,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const demandeAccord = this.createFromForm();
    if (demandeAccord.id !== undefined) {
      this.subscribeToSaveResponse(this.demandeAccordService.update(demandeAccord));
    } else {
      this.subscribeToSaveResponse(this.demandeAccordService.create(demandeAccord));
    }
  }

  private createFromForm(): IDemandeAccord {
    return {
      ...new DemandeAccord(),
      id: this.editForm.get(['id'])!.value,
      num: this.editForm.get(['num'])!.value,
      titre: this.editForm.get(['titre'])!.value,
      type: this.editForm.get(['type'])!.value,
      description: this.editForm.get(['description'])!.value,
      texteAccord: this.editForm.get(['texteAccord'])!.value,
      dateDemande: this.editForm.get(['dateDemande'])!.value,
      validation: this.editForm.get(['validation'])!.value,
      partenaire: this.editForm.get(['partenaire'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDemandeAccord>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IPartenaire): any {
    return item.id;
  }
}
