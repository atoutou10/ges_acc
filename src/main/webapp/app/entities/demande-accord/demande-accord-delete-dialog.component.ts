import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDemandeAccord } from 'app/shared/model/demande-accord.model';
import { DemandeAccordService } from './demande-accord.service';

@Component({
  templateUrl: './demande-accord-delete-dialog.component.html',
})
export class DemandeAccordDeleteDialogComponent {
  demandeAccord?: IDemandeAccord;

  constructor(
    protected demandeAccordService: DemandeAccordService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.demandeAccordService.delete(id).subscribe(() => {
      this.eventManager.broadcast('demandeAccordListModification');
      this.activeModal.close();
    });
  }
}
