import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDemandeAccord } from 'app/shared/model/demande-accord.model';
import { DemandeAccordService } from './demande-accord.service';
import { DemandeAccordDeleteDialogComponent } from './demande-accord-delete-dialog.component';

@Component({
  selector: 'jhi-demande-accord',
  templateUrl: './demande-accord.component.html',
})
export class DemandeAccordComponent implements OnInit, OnDestroy {
  demandeAccords?: IDemandeAccord[];
  eventSubscriber?: Subscription;

  constructor(
    protected demandeAccordService: DemandeAccordService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.demandeAccordService.query().subscribe((res: HttpResponse<IDemandeAccord[]>) => {
      this.demandeAccords = res.body ? res.body : [];
    });
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInDemandeAccords();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IDemandeAccord): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInDemandeAccords(): void {
    this.eventSubscriber = this.eventManager.subscribe('demandeAccordListModification', () => this.loadAll());
  }

  delete(demandeAccord: IDemandeAccord): void {
    const modalRef = this.modalService.open(DemandeAccordDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.demandeAccord = demandeAccord;
  }
}
