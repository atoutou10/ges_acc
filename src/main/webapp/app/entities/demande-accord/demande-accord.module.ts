import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GesAccSharedModule } from 'app/shared/shared.module';
import { DemandeAccordComponent } from './demande-accord.component';
import { DemandeAccordDetailComponent } from './demande-accord-detail.component';
import { DemandeAccordUpdateComponent } from './demande-accord-update.component';
import { DemandeAccordDeleteDialogComponent } from './demande-accord-delete-dialog.component';
import { demandeAccordRoute } from './demande-accord.route';

@NgModule({
  imports: [GesAccSharedModule, RouterModule.forChild(demandeAccordRoute)],
  declarations: [DemandeAccordComponent, DemandeAccordDetailComponent, DemandeAccordUpdateComponent, DemandeAccordDeleteDialogComponent],
  entryComponents: [DemandeAccordDeleteDialogComponent],
})
export class GesAccDemandeAccordModule {}
