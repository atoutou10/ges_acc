import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDemandeAccord } from 'app/shared/model/demande-accord.model';

@Component({
  selector: 'jhi-demande-accord-detail',
  templateUrl: './demande-accord-detail.component.html',
})
export class DemandeAccordDetailComponent implements OnInit {
  demandeAccord: IDemandeAccord | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ demandeAccord }) => {
      this.demandeAccord = demandeAccord;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
