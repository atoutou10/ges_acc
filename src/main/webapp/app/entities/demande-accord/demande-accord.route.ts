import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IDemandeAccord, DemandeAccord } from 'app/shared/model/demande-accord.model';
import { DemandeAccordService } from './demande-accord.service';
import { DemandeAccordComponent } from './demande-accord.component';
import { DemandeAccordDetailComponent } from './demande-accord-detail.component';
import { DemandeAccordUpdateComponent } from './demande-accord-update.component';

@Injectable({ providedIn: 'root' })
export class DemandeAccordResolve implements Resolve<IDemandeAccord> {
  constructor(private service: DemandeAccordService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDemandeAccord> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((demandeAccord: HttpResponse<DemandeAccord>) => {
          if (demandeAccord.body) {
            return of(demandeAccord.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DemandeAccord());
  }
}

export const demandeAccordRoute: Routes = [
  {
    path: '',
    component: DemandeAccordComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.demandeAccord.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DemandeAccordDetailComponent,
    resolve: {
      demandeAccord: DemandeAccordResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.demandeAccord.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DemandeAccordUpdateComponent,
    resolve: {
      demandeAccord: DemandeAccordResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.demandeAccord.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DemandeAccordUpdateComponent,
    resolve: {
      demandeAccord: DemandeAccordResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.demandeAccord.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
