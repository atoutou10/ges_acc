import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IParticipant, Participant } from 'app/shared/model/participant.model';
import { ParticipantService } from './participant.service';
import { IComite } from 'app/shared/model/comite.model';
import { ComiteService } from 'app/entities/comite/comite.service';

@Component({
  selector: 'jhi-participant-update',
  templateUrl: './participant-update.component.html',
})
export class ParticipantUpdateComponent implements OnInit {
  isSaving = false;
  comites: IComite[] = [];

  editForm = this.fb.group({
    id: [],
    nom: [],
    cin: [null, []],
    prenom: [],
    email: [],
    telephone: [],
    entreprise: [],
    poste: [],
    comite: [],
  });

  constructor(
    protected participantService: ParticipantService,
    protected comiteService: ComiteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ participant }) => {
      this.updateForm(participant);

      this.comiteService.query().subscribe((res: HttpResponse<IComite[]>) => (this.comites = res.body || []));
    });
  }

  updateForm(participant: IParticipant): void {
    this.editForm.patchValue({
      id: participant.id,
      nom: participant.nom,
      cin: participant.cin,
      prenom: participant.prenom,
      email: participant.email,
      telephone: participant.telephone,
      entreprise: participant.entreprise,
      poste: participant.poste,
      comite: participant.comite,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const participant = this.createFromForm();
    if (participant.id !== undefined) {
      this.subscribeToSaveResponse(this.participantService.update(participant));
    } else {
      this.subscribeToSaveResponse(this.participantService.create(participant));
    }
  }

  private createFromForm(): IParticipant {
    return {
      ...new Participant(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      cin: this.editForm.get(['cin'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      email: this.editForm.get(['email'])!.value,
      telephone: this.editForm.get(['telephone'])!.value,
      entreprise: this.editForm.get(['entreprise'])!.value,
      poste: this.editForm.get(['poste'])!.value,
      comite: this.editForm.get(['comite'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IParticipant>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IComite): any {
    return item.id;
  }
}
