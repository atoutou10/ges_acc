import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAvenant } from 'app/shared/model/avenant.model';
import { AvenantService } from './avenant.service';

@Component({
  templateUrl: './avenant-delete-dialog.component.html',
})
export class AvenantDeleteDialogComponent {
  avenant?: IAvenant;

  constructor(protected avenantService: AvenantService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.avenantService.delete(id).subscribe(() => {
      this.eventManager.broadcast('avenantListModification');
      this.activeModal.close();
    });
  }
}
