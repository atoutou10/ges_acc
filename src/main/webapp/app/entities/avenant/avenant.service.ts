import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAvenant } from 'app/shared/model/avenant.model';

type EntityResponseType = HttpResponse<IAvenant>;
type EntityArrayResponseType = HttpResponse<IAvenant[]>;

@Injectable({ providedIn: 'root' })
export class AvenantService {
  public resourceUrl = SERVER_API_URL + 'api/avenants';

  constructor(protected http: HttpClient) {}

  create(avenant: IAvenant): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(avenant);
    return this.http
      .post<IAvenant>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(avenant: IAvenant): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(avenant);
    return this.http
      .put<IAvenant>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAvenant>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAvenant[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(avenant: IAvenant): IAvenant {
    const copy: IAvenant = Object.assign({}, avenant, {
      dateSignature: avenant.dateSignature && avenant.dateSignature.isValid() ? avenant.dateSignature.format(DATE_FORMAT) : undefined,
      dateExpiration: avenant.dateExpiration && avenant.dateExpiration.isValid() ? avenant.dateExpiration.format(DATE_FORMAT) : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateSignature = res.body.dateSignature ? moment(res.body.dateSignature) : undefined;
      res.body.dateExpiration = res.body.dateExpiration ? moment(res.body.dateExpiration) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((avenant: IAvenant) => {
        avenant.dateSignature = avenant.dateSignature ? moment(avenant.dateSignature) : undefined;
        avenant.dateExpiration = avenant.dateExpiration ? moment(avenant.dateExpiration) : undefined;
      });
    }
    return res;
  }
}
