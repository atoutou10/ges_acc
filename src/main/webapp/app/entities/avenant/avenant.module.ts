import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GesAccSharedModule } from 'app/shared/shared.module';
import { AvenantComponent } from './avenant.component';
import { AvenantDetailComponent } from './avenant-detail.component';
import { AvenantUpdateComponent } from './avenant-update.component';
import { AvenantDeleteDialogComponent } from './avenant-delete-dialog.component';
import { avenantRoute } from './avenant.route';

@NgModule({
  imports: [GesAccSharedModule, RouterModule.forChild(avenantRoute)],
  declarations: [AvenantComponent, AvenantDetailComponent, AvenantUpdateComponent, AvenantDeleteDialogComponent],
  entryComponents: [AvenantDeleteDialogComponent],
})
export class GesAccAvenantModule {}
