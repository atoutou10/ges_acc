import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAvenant } from 'app/shared/model/avenant.model';

@Component({
  selector: 'jhi-avenant-detail',
  templateUrl: './avenant-detail.component.html',
})
export class AvenantDetailComponent implements OnInit {
  avenant: IAvenant | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ avenant }) => {
      this.avenant = avenant;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
