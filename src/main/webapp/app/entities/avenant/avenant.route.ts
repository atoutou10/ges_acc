import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAvenant, Avenant } from 'app/shared/model/avenant.model';
import { AvenantService } from './avenant.service';
import { AvenantComponent } from './avenant.component';
import { AvenantDetailComponent } from './avenant-detail.component';
import { AvenantUpdateComponent } from './avenant-update.component';

@Injectable({ providedIn: 'root' })
export class AvenantResolve implements Resolve<IAvenant> {
  constructor(private service: AvenantService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAvenant> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((avenant: HttpResponse<Avenant>) => {
          if (avenant.body) {
            return of(avenant.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Avenant());
  }
}

export const avenantRoute: Routes = [
  {
    path: '',
    component: AvenantComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.avenant.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AvenantDetailComponent,
    resolve: {
      avenant: AvenantResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.avenant.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AvenantUpdateComponent,
    resolve: {
      avenant: AvenantResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.avenant.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AvenantUpdateComponent,
    resolve: {
      avenant: AvenantResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.avenant.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
