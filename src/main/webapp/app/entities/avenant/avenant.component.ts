import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAvenant } from 'app/shared/model/avenant.model';
import { AvenantService } from './avenant.service';
import { AvenantDeleteDialogComponent } from './avenant-delete-dialog.component';

@Component({
  selector: 'jhi-avenant',
  templateUrl: './avenant.component.html',
})
export class AvenantComponent implements OnInit, OnDestroy {
  avenants?: IAvenant[];
  eventSubscriber?: Subscription;

  constructor(protected avenantService: AvenantService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.avenantService.query().subscribe((res: HttpResponse<IAvenant[]>) => {
      this.avenants = res.body ? res.body : [];
    });
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAvenants();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAvenant): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAvenants(): void {
    this.eventSubscriber = this.eventManager.subscribe('avenantListModification', () => this.loadAll());
  }

  delete(avenant: IAvenant): void {
    const modalRef = this.modalService.open(AvenantDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.avenant = avenant;
  }
}
