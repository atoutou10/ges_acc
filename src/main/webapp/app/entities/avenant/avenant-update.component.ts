import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';

import { IAvenant, Avenant } from 'app/shared/model/avenant.model';
import { AvenantService } from './avenant.service';

@Component({
  selector: 'jhi-avenant-update',
  templateUrl: './avenant-update.component.html',
})
export class AvenantUpdateComponent implements OnInit {
  isSaving = false;
  dateSignatureDp: any;
  dateExpirationDp: any;

  editForm = this.fb.group({
    id: [],
    libelle: [],
    document: [],
    but: [],
    description: [],
    dateSignature: [],
    dateExpiration: [],
    status: [],
  });

  constructor(protected avenantService: AvenantService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ avenant }) => {
      this.updateForm(avenant);
    });
  }

  updateForm(avenant: IAvenant): void {
    this.editForm.patchValue({
      id: avenant.id,
      libelle: avenant.libelle,
      document: avenant.document,
      but: avenant.but,
      description: avenant.description,
      dateSignature: avenant.dateSignature,
      dateExpiration: avenant.dateExpiration,
      status: avenant.status,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const avenant = this.createFromForm();
    if (avenant.id !== undefined) {
      this.subscribeToSaveResponse(this.avenantService.update(avenant));
    } else {
      this.subscribeToSaveResponse(this.avenantService.create(avenant));
    }
  }

  private createFromForm(): IAvenant {
    return {
      ...new Avenant(),
      id: this.editForm.get(['id'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
      document: this.editForm.get(['document'])!.value,
      but: this.editForm.get(['but'])!.value,
      description: this.editForm.get(['description'])!.value,
      dateSignature: this.editForm.get(['dateSignature'])!.value,
      dateExpiration: this.editForm.get(['dateExpiration'])!.value,
      status: this.editForm.get(['status'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAvenant>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
