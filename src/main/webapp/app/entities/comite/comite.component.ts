import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IComite } from 'app/shared/model/comite.model';
import { ComiteService } from './comite.service';
import { ComiteDeleteDialogComponent } from './comite-delete-dialog.component';

@Component({
  selector: 'jhi-comite',
  templateUrl: './comite.component.html',
})
export class ComiteComponent implements OnInit, OnDestroy {
  comites?: IComite[];
  eventSubscriber?: Subscription;

  constructor(protected comiteService: ComiteService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.comiteService.query().subscribe((res: HttpResponse<IComite[]>) => (this.comites = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInComites();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IComite): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInComites(): void {
    this.eventSubscriber = this.eventManager.subscribe('comiteListModification', () => this.loadAll());
  }

  delete(comite: IComite): void {
    const modalRef = this.modalService.open(ComiteDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.comite = comite;
  }
}
