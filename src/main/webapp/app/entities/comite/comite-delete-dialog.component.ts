import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IComite } from 'app/shared/model/comite.model';
import { ComiteService } from './comite.service';

@Component({
  templateUrl: './comite-delete-dialog.component.html',
})
export class ComiteDeleteDialogComponent {
  comite?: IComite;

  constructor(protected comiteService: ComiteService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.comiteService.delete(id).subscribe(() => {
      this.eventManager.broadcast('comiteListModification');
      this.activeModal.close();
    });
  }
}
