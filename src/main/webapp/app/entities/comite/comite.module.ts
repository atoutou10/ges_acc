import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GesAccSharedModule } from 'app/shared/shared.module';
import { ComiteComponent } from './comite.component';
import { ComiteDetailComponent } from './comite-detail.component';
import { ComiteUpdateComponent } from './comite-update.component';
import { ComiteDeleteDialogComponent } from './comite-delete-dialog.component';
import { comiteRoute } from './comite.route';

@NgModule({
  imports: [GesAccSharedModule, RouterModule.forChild(comiteRoute)],
  declarations: [ComiteComponent, ComiteDetailComponent, ComiteUpdateComponent, ComiteDeleteDialogComponent],
  entryComponents: [ComiteDeleteDialogComponent],
})
export class GesAccComiteModule {}
