import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IComite, Comite } from 'app/shared/model/comite.model';
import { ComiteService } from './comite.service';

@Component({
  selector: 'jhi-comite-update',
  templateUrl: './comite-update.component.html',
})
export class ComiteUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    role: [],
    nom: [],
  });

  constructor(protected comiteService: ComiteService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ comite }) => {
      this.updateForm(comite);
    });
  }

  updateForm(comite: IComite): void {
    this.editForm.patchValue({
      id: comite.id,
      role: comite.role,
      nom: comite.nom,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const comite = this.createFromForm();
    if (comite.id !== undefined) {
      this.subscribeToSaveResponse(this.comiteService.update(comite));
    } else {
      this.subscribeToSaveResponse(this.comiteService.create(comite));
    }
  }

  private createFromForm(): IComite {
    return {
      ...new Comite(),
      id: this.editForm.get(['id'])!.value,
      role: this.editForm.get(['role'])!.value,
      nom: this.editForm.get(['nom'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IComite>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
