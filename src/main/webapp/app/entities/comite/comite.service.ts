import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IComite } from 'app/shared/model/comite.model';

type EntityResponseType = HttpResponse<IComite>;
type EntityArrayResponseType = HttpResponse<IComite[]>;

@Injectable({ providedIn: 'root' })
export class ComiteService {
  public resourceUrl = SERVER_API_URL + 'api/comites';

  constructor(protected http: HttpClient) {}

  create(comite: IComite): Observable<EntityResponseType> {
    return this.http.post<IComite>(this.resourceUrl, comite, { observe: 'response' });
  }

  update(comite: IComite): Observable<EntityResponseType> {
    return this.http.put<IComite>(this.resourceUrl, comite, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IComite>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IComite[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
