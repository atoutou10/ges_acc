import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IComite } from 'app/shared/model/comite.model';

@Component({
  selector: 'jhi-comite-detail',
  templateUrl: './comite-detail.component.html',
})
export class ComiteDetailComponent implements OnInit {
  comite: IComite | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ comite }) => (this.comite = comite));
  }

  previousState(): void {
    window.history.back();
  }
}
