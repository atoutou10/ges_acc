import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IComite, Comite } from 'app/shared/model/comite.model';
import { ComiteService } from './comite.service';
import { ComiteComponent } from './comite.component';
import { ComiteDetailComponent } from './comite-detail.component';
import { ComiteUpdateComponent } from './comite-update.component';

@Injectable({ providedIn: 'root' })
export class ComiteResolve implements Resolve<IComite> {
  constructor(private service: ComiteService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IComite> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((comite: HttpResponse<Comite>) => {
          if (comite.body) {
            return of(comite.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Comite());
  }
}

export const comiteRoute: Routes = [
  {
    path: '',
    component: ComiteComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.comite.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ComiteDetailComponent,
    resolve: {
      comite: ComiteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.comite.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ComiteUpdateComponent,
    resolve: {
      comite: ComiteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.comite.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ComiteUpdateComponent,
    resolve: {
      comite: ComiteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gesAccApp.comite.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
