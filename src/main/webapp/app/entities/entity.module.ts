import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'accord',
        loadChildren: () => import('./accord/accord.module').then(m => m.GesAccAccordModule),
      },
      {
        path: 'partenaire',
        loadChildren: () => import('./partenaire/partenaire.module').then(m => m.GesAccPartenaireModule),
      },
      {
        path: 'type-partenaire',
        loadChildren: () => import('./type-partenaire/type-partenaire.module').then(m => m.GesAccTypePartenaireModule),
      },
      {
        path: 'comite',
        loadChildren: () => import('./comite/comite.module').then(m => m.GesAccComiteModule),
      },
      {
        path: 'activite',
        loadChildren: () => import('./activite/activite.module').then(m => m.GesAccActiviteModule),
      },
      {
        path: 'participant',
        loadChildren: () => import('./participant/participant.module').then(m => m.GesAccParticipantModule),
      },
      {
        path: 'avenant',
        loadChildren: () => import('./avenant/avenant.module').then(m => m.GesAccAvenantModule),
      },
      {
        path: 'demande-accord',
        loadChildren: () => import('./demande-accord/demande-accord.module').then(m => m.GesAccDemandeAccordModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class GesAccEntityModule {}
