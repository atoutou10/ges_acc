import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GesAccSharedModule } from 'app/shared/shared.module';

import { MessagesComponent } from './messages.component';
import { messagesRoute } from './messages.route';

@NgModule({
  imports: [GesAccSharedModule, RouterModule.forChild([messagesRoute])],
  declarations: [],
})
export class GesAccMessagesModule {}
