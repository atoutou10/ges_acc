import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';

import { DemandeAccordService } from 'app/entities/demande-accord/demande-accord.service';

//import { IDemandeAccord } from 'app/shared/model/demande-accord.model';

@Component({
  selector: 'jhi-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
})
export class MessagesComponent implements OnInit {
  demandeAccord: any;
  //numero : String = "";
  myDate = Date.now();

  constructor(protected demandeAccordService: DemandeAccordService, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.loadAll();

    /*  const id = this.activatedRoute.snapshot.paramMap.get['id'];
   this.demandeAccordService.find(id).subscribe(({ demandeAccord }) => {
      this.demandeAccord = demandeAccord;
    }); */
  }
  loadAll(): void {
    const id = parseInt(this.activatedRoute.snapshot.paramMap.get('id')!, 10);
    this.demandeAccordService.find(id).subscribe(demandeAccord => (this.demandeAccord = demandeAccord));
  }
}
