import { Route } from '@angular/router';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { MessagesComponent } from './messages.component';

export const messagesRoute: Route = {
  path: 'mails/:id',
  component: MessagesComponent,
  data: {
    authorities: [Authority.USER, Authority.ADMIN],
    pageTitle: 'Mails',
  },
  canActivate: [UserRouteAccessService],
};
