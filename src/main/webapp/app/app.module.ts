import { DashbordComponent } from './layouts/dashbord/dashbord.component';
import { SidebarComponent } from './layouts/sidebar/sidebar.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { GesAccSharedModule } from 'app/shared/shared.module';
import { GesAccCoreModule } from 'app/core/core.module';
import { GesAccAppRoutingModule } from './app-routing.module';
import { GesAccHomeModule } from './home/home.module';
import { GesAccMessagesModule } from './messages/messages.module';
import { GesAccEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { MessagesComponent } from './messages/messages.component';

@NgModule({
  imports: [
    BrowserModule,
    GesAccSharedModule,
    GesAccCoreModule,
    GesAccHomeModule,
    GesAccMessagesModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    GesAccEntityModule,
    GesAccAppRoutingModule,
  ],

  declarations: [
    MainComponent,
    NavbarComponent,
    SidebarComponent,
    ErrorComponent,
    PageRibbonComponent,
    ActiveMenuDirective,
    FooterComponent,
    DashbordComponent,
    MessagesComponent,
  ],
  bootstrap: [MainComponent],

  /* declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],*/
})
export class GesAccAppModule {}
