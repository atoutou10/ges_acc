/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.uvs.il.web.rest.vm;
