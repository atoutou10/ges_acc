package sn.uvs.il.web.rest;

import sn.uvs.il.domain.DemandeAccord;
import sn.uvs.il.repository.DemandeAccordRepository;
import sn.uvs.il.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.uvs.il.domain.DemandeAccord}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class DemandeAccordResource {

    private final Logger log = LoggerFactory.getLogger(DemandeAccordResource.class);

    private static final String ENTITY_NAME = "demandeAccord";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DemandeAccordRepository demandeAccordRepository;

    public DemandeAccordResource(DemandeAccordRepository demandeAccordRepository) {
        this.demandeAccordRepository = demandeAccordRepository;
    }

    /**
     * {@code POST  /demande-accords} : Create a new demandeAccord.
     *
     * @param demandeAccord the demandeAccord to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new demandeAccord, or with status {@code 400 (Bad Request)} if the demandeAccord has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/demande-accords")
    public ResponseEntity<DemandeAccord> createDemandeAccord(@RequestBody DemandeAccord demandeAccord) throws URISyntaxException {
        log.debug("REST request to save DemandeAccord : {}", demandeAccord);
        if (demandeAccord.getId() != null) {
            throw new BadRequestAlertException("A new demandeAccord cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DemandeAccord result = demandeAccordRepository.save(demandeAccord);
        return ResponseEntity.created(new URI("/api/demande-accords/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /demande-accords} : Updates an existing demandeAccord.
     *
     * @param demandeAccord the demandeAccord to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated demandeAccord,
     * or with status {@code 400 (Bad Request)} if the demandeAccord is not valid,
     * or with status {@code 500 (Internal Server Error)} if the demandeAccord couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/demande-accords")
    public ResponseEntity<DemandeAccord> updateDemandeAccord(@RequestBody DemandeAccord demandeAccord) throws URISyntaxException {
        log.debug("REST request to update DemandeAccord : {}", demandeAccord);
        if (demandeAccord.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DemandeAccord result = demandeAccordRepository.save(demandeAccord);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, demandeAccord.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /demande-accords} : get all the demandeAccords.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of demandeAccords in body.
     */
    @GetMapping("/demande-accords")
    public List<DemandeAccord> getAllDemandeAccords() {
        log.debug("REST request to get all DemandeAccords");
        return demandeAccordRepository.findAll();
    }

    /**
     * {@code GET  /demande-accords/:id} : get the "id" demandeAccord.
     *
     * @param id the id of the demandeAccord to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the demandeAccord, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/demande-accords/{id}")
    public ResponseEntity<DemandeAccord> getDemandeAccord(@PathVariable Long id) {
        log.debug("REST request to get DemandeAccord : {}", id);
        Optional<DemandeAccord> demandeAccord = demandeAccordRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(demandeAccord);
    }

    /**
     * {@code DELETE  /demande-accords/:id} : delete the "id" demandeAccord.
     *
     * @param id the id of the demandeAccord to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/demande-accords/{id}")
    public ResponseEntity<Void> deleteDemandeAccord(@PathVariable Long id) {
        log.debug("REST request to delete DemandeAccord : {}", id);
        demandeAccordRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
