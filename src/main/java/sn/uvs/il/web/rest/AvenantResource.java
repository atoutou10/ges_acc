package sn.uvs.il.web.rest;

import sn.uvs.il.domain.Avenant;
import sn.uvs.il.repository.AvenantRepository;
import sn.uvs.il.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.uvs.il.domain.Avenant}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AvenantResource {

    private final Logger log = LoggerFactory.getLogger(AvenantResource.class);

    private static final String ENTITY_NAME = "avenant";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AvenantRepository avenantRepository;

    public AvenantResource(AvenantRepository avenantRepository) {
        this.avenantRepository = avenantRepository;
    }

    /**
     * {@code POST  /avenants} : Create a new avenant.
     *
     * @param avenant the avenant to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new avenant, or with status {@code 400 (Bad Request)} if the avenant has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/avenants")
    public ResponseEntity<Avenant> createAvenant(@RequestBody Avenant avenant) throws URISyntaxException {
        log.debug("REST request to save Avenant : {}", avenant);
        if (avenant.getId() != null) {
            throw new BadRequestAlertException("A new avenant cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Avenant result = avenantRepository.save(avenant);
        return ResponseEntity.created(new URI("/api/avenants/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /avenants} : Updates an existing avenant.
     *
     * @param avenant the avenant to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated avenant,
     * or with status {@code 400 (Bad Request)} if the avenant is not valid,
     * or with status {@code 500 (Internal Server Error)} if the avenant couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/avenants")
    public ResponseEntity<Avenant> updateAvenant(@RequestBody Avenant avenant) throws URISyntaxException {
        log.debug("REST request to update Avenant : {}", avenant);
        if (avenant.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Avenant result = avenantRepository.save(avenant);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, avenant.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /avenants} : get all the avenants.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of avenants in body.
     */
    @GetMapping("/avenants")
    public List<Avenant> getAllAvenants() {
        log.debug("REST request to get all Avenants");
        return avenantRepository.findAll();
    }

    /**
     * {@code GET  /avenants/:id} : get the "id" avenant.
     *
     * @param id the id of the avenant to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the avenant, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/avenants/{id}")
    public ResponseEntity<Avenant> getAvenant(@PathVariable Long id) {
        log.debug("REST request to get Avenant : {}", id);
        Optional<Avenant> avenant = avenantRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(avenant);
    }

    /**
     * {@code DELETE  /avenants/:id} : delete the "id" avenant.
     *
     * @param id the id of the avenant to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/avenants/{id}")
    public ResponseEntity<Void> deleteAvenant(@PathVariable Long id) {
        log.debug("REST request to delete Avenant : {}", id);
        avenantRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
