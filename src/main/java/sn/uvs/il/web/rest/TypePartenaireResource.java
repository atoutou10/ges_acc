package sn.uvs.il.web.rest;

import sn.uvs.il.domain.TypePartenaire;
import sn.uvs.il.repository.TypePartenaireRepository;
import sn.uvs.il.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.uvs.il.domain.TypePartenaire}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class TypePartenaireResource {

    private final Logger log = LoggerFactory.getLogger(TypePartenaireResource.class);

    private static final String ENTITY_NAME = "typePartenaire";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TypePartenaireRepository typePartenaireRepository;

    public TypePartenaireResource(TypePartenaireRepository typePartenaireRepository) {
        this.typePartenaireRepository = typePartenaireRepository;
    }

    /**
     * {@code POST  /type-partenaires} : Create a new typePartenaire.
     *
     * @param typePartenaire the typePartenaire to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new typePartenaire, or with status {@code 400 (Bad Request)} if the typePartenaire has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/type-partenaires")
    public ResponseEntity<TypePartenaire> createTypePartenaire(@RequestBody TypePartenaire typePartenaire) throws URISyntaxException {
        log.debug("REST request to save TypePartenaire : {}", typePartenaire);
        if (typePartenaire.getId() != null) {
            throw new BadRequestAlertException("A new typePartenaire cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TypePartenaire result = typePartenaireRepository.save(typePartenaire);
        return ResponseEntity.created(new URI("/api/type-partenaires/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /type-partenaires} : Updates an existing typePartenaire.
     *
     * @param typePartenaire the typePartenaire to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated typePartenaire,
     * or with status {@code 400 (Bad Request)} if the typePartenaire is not valid,
     * or with status {@code 500 (Internal Server Error)} if the typePartenaire couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/type-partenaires")
    public ResponseEntity<TypePartenaire> updateTypePartenaire(@RequestBody TypePartenaire typePartenaire) throws URISyntaxException {
        log.debug("REST request to update TypePartenaire : {}", typePartenaire);
        if (typePartenaire.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TypePartenaire result = typePartenaireRepository.save(typePartenaire);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, typePartenaire.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /type-partenaires} : get all the typePartenaires.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of typePartenaires in body.
     */
    @GetMapping("/type-partenaires")
    public List<TypePartenaire> getAllTypePartenaires() {
        log.debug("REST request to get all TypePartenaires");
        return typePartenaireRepository.findAll();
    }

    /**
     * {@code GET  /type-partenaires/:id} : get the "id" typePartenaire.
     *
     * @param id the id of the typePartenaire to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the typePartenaire, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/type-partenaires/{id}")
    public ResponseEntity<TypePartenaire> getTypePartenaire(@PathVariable Long id) {
        log.debug("REST request to get TypePartenaire : {}", id);
        Optional<TypePartenaire> typePartenaire = typePartenaireRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(typePartenaire);
    }

    /**
     * {@code DELETE  /type-partenaires/:id} : delete the "id" typePartenaire.
     *
     * @param id the id of the typePartenaire to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/type-partenaires/{id}")
    public ResponseEntity<Void> deleteTypePartenaire(@PathVariable Long id) {
        log.debug("REST request to delete TypePartenaire : {}", id);
        typePartenaireRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
