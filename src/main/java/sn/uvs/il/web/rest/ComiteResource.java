package sn.uvs.il.web.rest;

import sn.uvs.il.domain.Comite;
import sn.uvs.il.repository.ComiteRepository;
import sn.uvs.il.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing {@link sn.uvs.il.domain.Comite}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ComiteResource {

    private final Logger log = LoggerFactory.getLogger(ComiteResource.class);

    private static final String ENTITY_NAME = "comite";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ComiteRepository comiteRepository;

    public ComiteResource(ComiteRepository comiteRepository) {
        this.comiteRepository = comiteRepository;
    }

    /**
     * {@code POST  /comites} : Create a new comite.
     *
     * @param comite the comite to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new comite, or with status {@code 400 (Bad Request)} if the comite has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/comites")
    public ResponseEntity<Comite> createComite(@RequestBody Comite comite) throws URISyntaxException {
        log.debug("REST request to save Comite : {}", comite);
        if (comite.getId() != null) {
            throw new BadRequestAlertException("A new comite cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Comite result = comiteRepository.save(comite);
        return ResponseEntity.created(new URI("/api/comites/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /comites} : Updates an existing comite.
     *
     * @param comite the comite to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated comite,
     * or with status {@code 400 (Bad Request)} if the comite is not valid,
     * or with status {@code 500 (Internal Server Error)} if the comite couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/comites")
    public ResponseEntity<Comite> updateComite(@RequestBody Comite comite) throws URISyntaxException {
        log.debug("REST request to update Comite : {}", comite);
        if (comite.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Comite result = comiteRepository.save(comite);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, comite.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /comites} : get all the comites.
     *
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of comites in body.
     */
    @GetMapping("/comites")
    public List<Comite> getAllComites(@RequestParam(required = false) String filter) {
        if ("accord-is-null".equals(filter)) {
            log.debug("REST request to get all Comites where accord is null");
            return StreamSupport
                .stream(comiteRepository.findAll().spliterator(), false)
                .filter(comite -> comite.getAccord() == null)
                .collect(Collectors.toList());
        }
        log.debug("REST request to get all Comites");
        return comiteRepository.findAll();
    }

    /**
     * {@code GET  /comites/:id} : get the "id" comite.
     *
     * @param id the id of the comite to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the comite, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/comites/{id}")
    public ResponseEntity<Comite> getComite(@PathVariable Long id) {
        log.debug("REST request to get Comite : {}", id);
        Optional<Comite> comite = comiteRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(comite);
    }

    /**
     * {@code DELETE  /comites/:id} : delete the "id" comite.
     *
     * @param id the id of the comite to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/comites/{id}")
    public ResponseEntity<Void> deleteComite(@PathVariable Long id) {
        log.debug("REST request to delete Comite : {}", id);
        comiteRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
