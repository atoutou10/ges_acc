package sn.uvs.il.repository;

import sn.uvs.il.domain.DemandeAccord;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DemandeAccord entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DemandeAccordRepository extends JpaRepository<DemandeAccord, Long> {

}
