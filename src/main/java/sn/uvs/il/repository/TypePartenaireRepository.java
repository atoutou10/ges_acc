package sn.uvs.il.repository;

import sn.uvs.il.domain.TypePartenaire;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TypePartenaire entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypePartenaireRepository extends JpaRepository<TypePartenaire, Long> {
}
