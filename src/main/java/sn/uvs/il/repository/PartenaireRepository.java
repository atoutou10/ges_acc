package sn.uvs.il.repository;

import sn.uvs.il.domain.Partenaire;

import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Partenaire entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PartenaireRepository extends JpaRepository<Partenaire, Long> {
    @Query(
        value="SELECT * FROM type_partenaire t , partenaire p WHERE t.id = p.typepartenaire_id and t.libelle=?1 ",
        nativeQuery = true
        )
   List<Partenaire> findByTypePartenaire(String status);
}
