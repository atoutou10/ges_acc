package sn.uvs.il.repository;

import sn.uvs.il.domain.Comite;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Comite entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ComiteRepository extends JpaRepository<Comite, Long> {
}
