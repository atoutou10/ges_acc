package sn.uvs.il.repository;

import sn.uvs.il.domain.Accord;

import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Accord entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AccordRepository extends JpaRepository<Accord, Long> {
    List<Accord> findByStatusTrue();
    List<Accord> findByStatusFalse();
}
