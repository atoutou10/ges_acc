package sn.uvs.il.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Comite.
 */
@Entity
@Table(name = "comite")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Comite implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "role")
    private String role;

    @Column(name = "nom")
    private String nom;

    @OneToMany(mappedBy = "comite")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Participant> participants = new HashSet<>();

    @OneToOne(mappedBy = "comite")
    @JsonIgnore
    private Accord accord;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public Comite role(String role) {
        this.role = role;
        return this;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getNom() {
        return nom;
    }

    public Comite nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Participant> getParticipants() {
        return participants;
    }

    public Comite participants(Set<Participant> participants) {
        this.participants = participants;
        return this;
    }

    public Comite addParticipant(Participant participant) {
        this.participants.add(participant);
        participant.setComite(this);
        return this;
    }

    public Comite removeParticipant(Participant participant) {
        this.participants.remove(participant);
        participant.setComite(null);
        return this;
    }

    public void setParticipants(Set<Participant> participants) {
        this.participants = participants;
    }

    public Accord getAccord() {
        return accord;
    }

    public Comite accord(Accord accord) {
        this.accord = accord;
        return this;
    }

    public void setAccord(Accord accord) {
        this.accord = accord;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Comite)) {
            return false;
        }
        return id != null && id.equals(((Comite) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Comite{" +
            "id=" + getId() +
            ", role='" + getRole() + "'" +
            ", nom='" + getNom() + "'" +
            "}";
    }
}
