package sn.uvs.il.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Activite.
 */
@Entity
@Table(name = "activite")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Activite implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "photo")
    private String photo;

    @Column(name = "titre")
    private String titre;

    @Column(name = "soustitre")
    private String soustitre;

    @Column(name = "objectif")
    private String objectif;

    @Column(name = "description")
    private String description;

    @Column(name = "datedeb")
    private LocalDate datedeb;

    @Column(name = "datefin")
    private LocalDate datefin;

    @Column(name = "status")
    private Boolean status;

    @ManyToOne
    @JsonIgnoreProperties(value = "activites", allowSetters = true)
    private Accord accord;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public Activite photo(String photo) {
        this.photo = photo;
        return this;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTitre() {
        return titre;
    }

    public Activite titre(String titre) {
        this.titre = titre;
        return this;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getSoustitre() {
        return soustitre;
    }

    public Activite soustitre(String soustitre) {
        this.soustitre = soustitre;
        return this;
    }

    public void setSoustitre(String soustitre) {
        this.soustitre = soustitre;
    }

    public String getObjectif() {
        return objectif;
    }

    public Activite objectif(String objectif) {
        this.objectif = objectif;
        return this;
    }

    public void setObjectif(String objectif) {
        this.objectif = objectif;
    }

    public String getDescription() {
        return description;
    }

    public Activite description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDatedeb() {
        return datedeb;
    }

    public Activite datedeb(LocalDate datedeb) {
        this.datedeb = datedeb;
        return this;
    }

    public void setDatedeb(LocalDate datedeb) {
        this.datedeb = datedeb;
    }

    public LocalDate getDatefin() {
        return datefin;
    }

    public Activite datefin(LocalDate datefin) {
        this.datefin = datefin;
        return this;
    }

    public void setDatefin(LocalDate datefin) {
        this.datefin = datefin;
    }

    public Boolean isStatus() {
        return status;
    }

    public Activite status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Accord getAccord() {
        return accord;
    }

    public Activite accord(Accord accord) {
        this.accord = accord;
        return this;
    }

    public void setAccord(Accord accord) {
        this.accord = accord;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Activite)) {
            return false;
        }
        return id != null && id.equals(((Activite) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Activite{" +
            "id=" + getId() +
            ", photo='" + getPhoto() + "'" +
            ", titre='" + getTitre() + "'" +
            ", soustitre='" + getSoustitre() + "'" +
            ", objectif='" + getObjectif() + "'" +
            ", description='" + getDescription() + "'" +
            ", datedeb='" + getDatedeb() + "'" +
            ", datefin='" + getDatefin() + "'" +
            ", status='" + isStatus() + "'" +
            "}";
    }
}
