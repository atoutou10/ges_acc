package sn.uvs.il.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Accord.
 */
@Entity
@Table(name = "accord")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Accord implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "titre")
    private String titre;

    @Column(name = "domaine")
    private String domaine;

    @Column(name = "description")
    private String description;

    @Column(name = "but")
    private String but;

    @Column(name = "date_signature")
    private LocalDate dateSignature;

    @Column(name = "date_expiration")
    private LocalDate dateExpiration;

    @Column(name = "status")
    private Boolean status;

    @OneToOne
    @JoinColumn(unique = true)
    private Comite comite;

    @OneToMany(mappedBy = "accord")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Activite> activites = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "accords", allowSetters = true)
    private Avenant avenant;

    @ManyToOne
    @JsonIgnoreProperties(value = "accords", allowSetters = true)
    private Partenaire partenaire;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Accord nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTitre() {
        return titre;
    }

    public Accord titre(String titre) {
        this.titre = titre;
        return this;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDomaine() {
        return domaine;
    }

    public Accord domaine(String domaine) {
        this.domaine = domaine;
        return this;
    }

    public void setDomaine(String domaine) {
        this.domaine = domaine;
    }

    public String getDescription() {
        return description;
    }

    public Accord description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBut() {
        return but;
    }

    public Accord but(String but) {
        this.but = but;
        return this;
    }

    public void setBut(String but) {
        this.but = but;
    }

    public LocalDate getDateSignature() {
        return dateSignature;
    }

    public Accord dateSignature(LocalDate dateSignature) {
        this.dateSignature = dateSignature;
        return this;
    }

    public void setDateSignature(LocalDate dateSignature) {
        this.dateSignature = dateSignature;
    }

    public LocalDate getDateExpiration() {
        return dateExpiration;
    }

    public Accord dateExpiration(LocalDate dateExpiration) {
        this.dateExpiration = dateExpiration;
        return this;
    }

    public void setDateExpiration(LocalDate dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public Boolean isStatus() {
        return status;
    }

    public Accord status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Comite getComite() {
        return comite;
    }

    public Accord comite(Comite comite) {
        this.comite = comite;
        return this;
    }

    public void setComite(Comite comite) {
        this.comite = comite;
    }

    public Set<Activite> getActivites() {
        return activites;
    }

    public Accord activites(Set<Activite> activites) {
        this.activites = activites;
        return this;
    }

    public Accord addActivite(Activite activite) {
        this.activites.add(activite);
        activite.setAccord(this);
        return this;
    }

    public Accord removeActivite(Activite activite) {
        this.activites.remove(activite);
        activite.setAccord(null);
        return this;
    }

    public void setActivites(Set<Activite> activites) {
        this.activites = activites;
    }

    public Avenant getAvenant() {
        return avenant;
    }

    public Accord avenant(Avenant avenant) {
        this.avenant = avenant;
        return this;
    }

    public void setAvenant(Avenant avenant) {
        this.avenant = avenant;
    }

    public Partenaire getPartenaire() {
        return partenaire;
    }

    public Accord partenaire(Partenaire partenaire) {
        this.partenaire = partenaire;
        return this;
    }

    public void setPartenaire(Partenaire partenaire) {
        this.partenaire = partenaire;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Accord)) {
            return false;
        }
        return id != null && id.equals(((Accord) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Accord{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", titre='" + getTitre() + "'" +
            ", domaine='" + getDomaine() + "'" +
            ", description='" + getDescription() + "'" +
            ", but='" + getBut() + "'" +
            ", dateSignature='" + getDateSignature() + "'" +
            ", dateExpiration='" + getDateExpiration() + "'" +
            ", status='" + isStatus() + "'" +
            "}";
    }
}
