package sn.uvs.il.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A DemandeAccord.
 */
@Entity
@Table(name = "demande_accord")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class DemandeAccord implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "num")
   /* @GeneratedValue(generator = "prod-generator")
    @GenericGenerator(name = "prod-generator",
        parameters = @Parameter(name = "prefix", value = "request"),
        strategy = "com.baeldung.hibernate.pojo.generator.MyGenerator")*/
    private String num;

    @Column(name = "titre")
    private String titre;

    @Column(name = "type")
    private String type;

    @Column(name = "description")
    private String description;

    @Column(name = "texte_accord")
    private String texteAccord;

    @Column(name = "date_demande")
    private LocalDate dateDemande;

    @Column(name = "validation")
    private Boolean validation;

    @ManyToOne
    @JsonIgnoreProperties(value = "demandes", allowSetters = true)
    private Partenaire partenaire;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNum() {
        return num;
    }

    public DemandeAccord num(String num) {
        this.num = num;
        return this;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getTitre() {
        return titre;
    }

    public DemandeAccord titre(String titre) {
        this.titre = titre;
        return this;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getType() {
        return type;
    }

    public DemandeAccord type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public DemandeAccord description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTexteAccord() {
        return texteAccord;
    }

    public DemandeAccord texteAccord(String texteAccord) {
        this.texteAccord = texteAccord;
        return this;
    }

    public void setTexteAccord(String texteAccord) {
        this.texteAccord = texteAccord;
    }

    public LocalDate getDateDemande() {
        return dateDemande;
    }

    public DemandeAccord dateDemande(LocalDate dateDemande) {
        this.dateDemande = dateDemande;
        return this;
    }

    public void setDateDemande(LocalDate dateDemande) {
        this.dateDemande = dateDemande;
    }

    public Boolean isValidation() {
        return validation;
    }

    public DemandeAccord validation(Boolean validation) {
        this.validation = validation;
        return this;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public Partenaire getPartenaire() {
        return partenaire;
    }

    public DemandeAccord partenaire(Partenaire partenaire) {
        this.partenaire = partenaire;
        return this;
    }

    public void setPartenaire(Partenaire partenaire) {
        this.partenaire = partenaire;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DemandeAccord)) {
            return false;
        }
        return id != null && id.equals(((DemandeAccord) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DemandeAccord{" +
            "id=" + getId() +
            ", num='" + getNum() + "'" +
            ", titre='" + getTitre() + "'" +
            ", type='" + getType() + "'" +
            ", description='" + getDescription() + "'" +
            ", texteAccord='" + getTexteAccord() + "'" +
            ", dateDemande='" + getDateDemande() + "'" +
            ", validation='" + isValidation() + "'" +
            "}";
    }
}
