package sn.uvs.il.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Avenant.
 */
@Entity
@Table(name = "avenant")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Avenant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "document")
    private String document;

    @Column(name = "but")
    private String but;

    @Column(name = "description")
    private String description;

    @Column(name = "date_signature")
    private LocalDate dateSignature;

    @Column(name = "date_expiration")
    private LocalDate dateExpiration;

    @Column(name = "status")
    private Boolean status;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public Avenant libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDocument() {
        return document;
    }

    public Avenant document(String document) {
        this.document = document;
        return this;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getBut() {
        return but;
    }

    public Avenant but(String but) {
        this.but = but;
        return this;
    }

    public void setBut(String but) {
        this.but = but;
    }

    public String getDescription() {
        return description;
    }

    public Avenant description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDateSignature() {
        return dateSignature;
    }

    public Avenant dateSignature(LocalDate dateSignature) {
        this.dateSignature = dateSignature;
        return this;
    }

    public void setDateSignature(LocalDate dateSignature) {
        this.dateSignature = dateSignature;
    }

    public LocalDate getDateExpiration() {
        return dateExpiration;
    }

    public Avenant dateExpiration(LocalDate dateExpiration) {
        this.dateExpiration = dateExpiration;
        return this;
    }

    public void setDateExpiration(LocalDate dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public Boolean isStatus() {
        return status;
    }

    public Avenant status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Avenant)) {
            return false;
        }
        return id != null && id.equals(((Avenant) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Avenant{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", document='" + getDocument() + "'" +
            ", but='" + getBut() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateSignature='" + getDateSignature() + "'" +
            ", dateExpiration='" + getDateExpiration() + "'" +
            ", status='" + isStatus() + "'" +
            "}";
    }
}
