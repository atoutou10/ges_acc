package sn.uvs.il.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Partenaire.
 */
@Entity
@Table(name = "partenaire")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Partenaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "domaine")
    private String domaine;

    @Column(name = "pays")
    private String pays;

    @OneToMany(mappedBy = "partenaire")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Accord> accords = new HashSet<>();

    @OneToMany(mappedBy = "partenaire")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<DemandeAccord> demandes = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "partenaires", allowSetters = true)
    private TypePartenaire typepartenaire;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Partenaire nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDomaine() {
        return domaine;
    }

    public Partenaire domaine(String domaine) {
        this.domaine = domaine;
        return this;
    }

    public void setDomaine(String domaine) {
        this.domaine = domaine;
    }

    public String getPays() {
        return pays;
    }

    public Partenaire pays(String pays) {
        this.pays = pays;
        return this;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public Set<Accord> getAccords() {
        return accords;
    }

    public Partenaire accords(Set<Accord> accords) {
        this.accords = accords;
        return this;
    }

    public Partenaire addAccord(Accord accord) {
        this.accords.add(accord);
        accord.setPartenaire(this);
        return this;
    }

    public Partenaire removeAccord(Accord accord) {
        this.accords.remove(accord);
        accord.setPartenaire(null);
        return this;
    }

    public void setAccords(Set<Accord> accords) {
        this.accords = accords;
    }

    public Set<DemandeAccord> getDemandes() {
        return demandes;
    }

    public Partenaire demandes(Set<DemandeAccord> demandeAccords) {
        this.demandes = demandeAccords;
        return this;
    }

    public Partenaire addDemande(DemandeAccord demandeAccord) {
        this.demandes.add(demandeAccord);
        demandeAccord.setPartenaire(this);
        return this;
    }

    public Partenaire removeDemande(DemandeAccord demandeAccord) {
        this.demandes.remove(demandeAccord);
        demandeAccord.setPartenaire(null);
        return this;
    }

    public void setDemandes(Set<DemandeAccord> demandeAccords) {
        this.demandes = demandeAccords;
    }

    public TypePartenaire getTypepartenaire() {
        return typepartenaire;
    }

    public Partenaire typepartenaire(TypePartenaire typePartenaire) {
        this.typepartenaire = typePartenaire;
        return this;
    }

    public void setTypepartenaire(TypePartenaire typePartenaire) {
        this.typepartenaire = typePartenaire;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Partenaire)) {
            return false;
        }
        return id != null && id.equals(((Partenaire) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Partenaire{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", domaine='" + getDomaine() + "'" +
            ", pays='" + getPays() + "'" +
            "}";
    }
}
