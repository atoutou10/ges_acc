package sn.uvs.il.web.rest;

import sn.uvs.il.GesAccApp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Test class for the FooResource REST controller.
 *
 * @see FooResource
 */
@SpringBootTest(classes = GesAccApp.class)
public class FooResourceIT {

    private MockMvc restMockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        FooResource fooResource = new FooResource();
        restMockMvc = MockMvcBuilders
            .standaloneSetup(fooResource)
            .build();
    }

    /**
     * Test status
     */
    @Test
    public void testStatus() throws Exception {
        restMockMvc.perform(post("/api/foo/status"))
            .andExpect(status().isOk());
    }

    /**
     * Test voir
     */
    @Test
    public void testVoir() throws Exception {
        restMockMvc.perform(get("/api/foo/voir"))
            .andExpect(status().isOk());
    }
}
