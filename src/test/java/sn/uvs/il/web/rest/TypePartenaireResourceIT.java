package sn.uvs.il.web.rest;

import sn.uvs.il.GesAccApp;
import sn.uvs.il.domain.TypePartenaire;
import sn.uvs.il.repository.TypePartenaireRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TypePartenaireResource} REST controller.
 */
@SpringBootTest(classes = GesAccApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class TypePartenaireResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    @Autowired
    private TypePartenaireRepository typePartenaireRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTypePartenaireMockMvc;

    private TypePartenaire typePartenaire;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TypePartenaire createEntity(EntityManager em) {
        TypePartenaire typePartenaire = new TypePartenaire()
            .libelle(DEFAULT_LIBELLE);
        return typePartenaire;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TypePartenaire createUpdatedEntity(EntityManager em) {
        TypePartenaire typePartenaire = new TypePartenaire()
            .libelle(UPDATED_LIBELLE);
        return typePartenaire;
    }

    @BeforeEach
    public void initTest() {
        typePartenaire = createEntity(em);
    }

    @Test
    @Transactional
    public void createTypePartenaire() throws Exception {
        int databaseSizeBeforeCreate = typePartenaireRepository.findAll().size();
        // Create the TypePartenaire
        restTypePartenaireMockMvc.perform(post("/api/type-partenaires")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typePartenaire)))
            .andExpect(status().isCreated());

        // Validate the TypePartenaire in the database
        List<TypePartenaire> typePartenaireList = typePartenaireRepository.findAll();
        assertThat(typePartenaireList).hasSize(databaseSizeBeforeCreate + 1);
        TypePartenaire testTypePartenaire = typePartenaireList.get(typePartenaireList.size() - 1);
        assertThat(testTypePartenaire.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
    }

    @Test
    @Transactional
    public void createTypePartenaireWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = typePartenaireRepository.findAll().size();

        // Create the TypePartenaire with an existing ID
        typePartenaire.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTypePartenaireMockMvc.perform(post("/api/type-partenaires")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typePartenaire)))
            .andExpect(status().isBadRequest());

        // Validate the TypePartenaire in the database
        List<TypePartenaire> typePartenaireList = typePartenaireRepository.findAll();
        assertThat(typePartenaireList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTypePartenaires() throws Exception {
        // Initialize the database
        typePartenaireRepository.saveAndFlush(typePartenaire);

        // Get all the typePartenaireList
        restTypePartenaireMockMvc.perform(get("/api/type-partenaires?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(typePartenaire.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)));
    }
    
    @Test
    @Transactional
    public void getTypePartenaire() throws Exception {
        // Initialize the database
        typePartenaireRepository.saveAndFlush(typePartenaire);

        // Get the typePartenaire
        restTypePartenaireMockMvc.perform(get("/api/type-partenaires/{id}", typePartenaire.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(typePartenaire.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE));
    }
    @Test
    @Transactional
    public void getNonExistingTypePartenaire() throws Exception {
        // Get the typePartenaire
        restTypePartenaireMockMvc.perform(get("/api/type-partenaires/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTypePartenaire() throws Exception {
        // Initialize the database
        typePartenaireRepository.saveAndFlush(typePartenaire);

        int databaseSizeBeforeUpdate = typePartenaireRepository.findAll().size();

        // Update the typePartenaire
        TypePartenaire updatedTypePartenaire = typePartenaireRepository.findById(typePartenaire.getId()).get();
        // Disconnect from session so that the updates on updatedTypePartenaire are not directly saved in db
        em.detach(updatedTypePartenaire);
        updatedTypePartenaire
            .libelle(UPDATED_LIBELLE);

        restTypePartenaireMockMvc.perform(put("/api/type-partenaires")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedTypePartenaire)))
            .andExpect(status().isOk());

        // Validate the TypePartenaire in the database
        List<TypePartenaire> typePartenaireList = typePartenaireRepository.findAll();
        assertThat(typePartenaireList).hasSize(databaseSizeBeforeUpdate);
        TypePartenaire testTypePartenaire = typePartenaireList.get(typePartenaireList.size() - 1);
        assertThat(testTypePartenaire.getLibelle()).isEqualTo(UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    public void updateNonExistingTypePartenaire() throws Exception {
        int databaseSizeBeforeUpdate = typePartenaireRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTypePartenaireMockMvc.perform(put("/api/type-partenaires")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(typePartenaire)))
            .andExpect(status().isBadRequest());

        // Validate the TypePartenaire in the database
        List<TypePartenaire> typePartenaireList = typePartenaireRepository.findAll();
        assertThat(typePartenaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTypePartenaire() throws Exception {
        // Initialize the database
        typePartenaireRepository.saveAndFlush(typePartenaire);

        int databaseSizeBeforeDelete = typePartenaireRepository.findAll().size();

        // Delete the typePartenaire
        restTypePartenaireMockMvc.perform(delete("/api/type-partenaires/{id}", typePartenaire.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TypePartenaire> typePartenaireList = typePartenaireRepository.findAll();
        assertThat(typePartenaireList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
