package sn.uvs.il.web.rest;

import sn.uvs.il.GesAccApp;
import sn.uvs.il.domain.DemandeAccord;
import sn.uvs.il.repository.DemandeAccordRepository;
import sn.uvs.il.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static sn.uvs.il.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DemandeAccordResource} REST controller.
 */
@SpringBootTest(classes = GesAccApp.class)
//<<<<<<< HEAD
//=======
@AutoConfigureMockMvc
@WithMockUser
//>>>>>>> jhipster_upgrade
public class DemandeAccordResourceIT {

    private static final String DEFAULT_NUM = "AAAAAAAAAA";
    private static final String UPDATED_NUM = "BBBBBBBBBB";

    private static final String DEFAULT_TITRE = "AAAAAAAAAA";
    private static final String UPDATED_TITRE = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TEXTE_ACCORD = "AAAAAAAAAA";
    private static final String UPDATED_TEXTE_ACCORD = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_DEMANDE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DEMANDE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_VALIDATION = false;
    private static final Boolean UPDATED_VALIDATION = true;

    @Autowired
    private DemandeAccordRepository demandeAccordRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDemandeAccordMockMvc;

    private DemandeAccord demandeAccord;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DemandeAccordResource demandeAccordResource = new DemandeAccordResource(demandeAccordRepository);
        this.restDemandeAccordMockMvc = MockMvcBuilders.standaloneSetup(demandeAccordResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DemandeAccord createEntity(EntityManager em) {
        DemandeAccord demandeAccord = new DemandeAccord()
            .num(DEFAULT_NUM)
            .titre(DEFAULT_TITRE)
            .type(DEFAULT_TYPE)
            .description(DEFAULT_DESCRIPTION)
            .texteAccord(DEFAULT_TEXTE_ACCORD)
            .dateDemande(DEFAULT_DATE_DEMANDE)
            .validation(DEFAULT_VALIDATION);
        return demandeAccord;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DemandeAccord createUpdatedEntity(EntityManager em) {
        DemandeAccord demandeAccord = new DemandeAccord()
            .num(UPDATED_NUM)
            .titre(UPDATED_TITRE)
            .type(UPDATED_TYPE)
            .description(UPDATED_DESCRIPTION)
            .texteAccord(UPDATED_TEXTE_ACCORD)
            .dateDemande(UPDATED_DATE_DEMANDE)
            .validation(UPDATED_VALIDATION);
        return demandeAccord;
    }

    @BeforeEach
    public void initTest() {
        demandeAccord = createEntity(em);
    }

    @Test
    @Transactional
    public void createDemandeAccord() throws Exception {
        int databaseSizeBeforeCreate = demandeAccordRepository.findAll().size();
        // Create the DemandeAccord
        restDemandeAccordMockMvc.perform(post("/api/demande-accords")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(demandeAccord)))
            .andExpect(status().isCreated());

        // Validate the DemandeAccord in the database
        List<DemandeAccord> demandeAccordList = demandeAccordRepository.findAll();
        assertThat(demandeAccordList).hasSize(databaseSizeBeforeCreate + 1);
        DemandeAccord testDemandeAccord = demandeAccordList.get(demandeAccordList.size() - 1);
        assertThat(testDemandeAccord.getNum()).isEqualTo(DEFAULT_NUM);
        assertThat(testDemandeAccord.getTitre()).isEqualTo(DEFAULT_TITRE);
        assertThat(testDemandeAccord.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testDemandeAccord.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDemandeAccord.getTexteAccord()).isEqualTo(DEFAULT_TEXTE_ACCORD);
        assertThat(testDemandeAccord.getDateDemande()).isEqualTo(DEFAULT_DATE_DEMANDE);
        assertThat(testDemandeAccord.isValidation()).isEqualTo(DEFAULT_VALIDATION);
    }

    @Test
    @Transactional
    public void createDemandeAccordWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = demandeAccordRepository.findAll().size();

        // Create the DemandeAccord with an existing ID
        demandeAccord.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDemandeAccordMockMvc.perform(post("/api/demande-accords")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(demandeAccord)))
            .andExpect(status().isBadRequest());

        // Validate the DemandeAccord in the database
        List<DemandeAccord> demandeAccordList = demandeAccordRepository.findAll();
        assertThat(demandeAccordList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDemandeAccords() throws Exception {
        // Initialize the database
        demandeAccordRepository.saveAndFlush(demandeAccord);

        // Get all the demandeAccordList
        restDemandeAccordMockMvc.perform(get("/api/demande-accords?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(demandeAccord.getId().intValue())))
            .andExpect(jsonPath("$.[*].num").value(hasItem(DEFAULT_NUM)))
            .andExpect(jsonPath("$.[*].titre").value(hasItem(DEFAULT_TITRE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].texteAccord").value(hasItem(DEFAULT_TEXTE_ACCORD)))
            .andExpect(jsonPath("$.[*].dateDemande").value(hasItem(DEFAULT_DATE_DEMANDE.toString())))
            .andExpect(jsonPath("$.[*].validation").value(hasItem(DEFAULT_VALIDATION.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getDemandeAccord() throws Exception {
        // Initialize the database
        demandeAccordRepository.saveAndFlush(demandeAccord);

        // Get the demandeAccord
        restDemandeAccordMockMvc.perform(get("/api/demande-accords/{id}", demandeAccord.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(demandeAccord.getId().intValue()))
            .andExpect(jsonPath("$.num").value(DEFAULT_NUM))
            .andExpect(jsonPath("$.titre").value(DEFAULT_TITRE))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.texteAccord").value(DEFAULT_TEXTE_ACCORD))
            .andExpect(jsonPath("$.dateDemande").value(DEFAULT_DATE_DEMANDE.toString()))
            .andExpect(jsonPath("$.validation").value(DEFAULT_VALIDATION.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingDemandeAccord() throws Exception {
        // Get the demandeAccord
        restDemandeAccordMockMvc.perform(get("/api/demande-accords/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDemandeAccord() throws Exception {
        // Initialize the database
        demandeAccordRepository.saveAndFlush(demandeAccord);

        int databaseSizeBeforeUpdate = demandeAccordRepository.findAll().size();

        // Update the demandeAccord
        DemandeAccord updatedDemandeAccord = demandeAccordRepository.findById(demandeAccord.getId()).get();
        // Disconnect from session so that the updates on updatedDemandeAccord are not directly saved in db
        em.detach(updatedDemandeAccord);
        updatedDemandeAccord
            .num(UPDATED_NUM)
            .titre(UPDATED_TITRE)
            .type(UPDATED_TYPE)
            .description(UPDATED_DESCRIPTION)
            .texteAccord(UPDATED_TEXTE_ACCORD)
            .dateDemande(UPDATED_DATE_DEMANDE)
            .validation(UPDATED_VALIDATION);

        restDemandeAccordMockMvc.perform(put("/api/demande-accords")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedDemandeAccord)))
            .andExpect(status().isOk());

        // Validate the DemandeAccord in the database
        List<DemandeAccord> demandeAccordList = demandeAccordRepository.findAll();
        assertThat(demandeAccordList).hasSize(databaseSizeBeforeUpdate);
        DemandeAccord testDemandeAccord = demandeAccordList.get(demandeAccordList.size() - 1);
        assertThat(testDemandeAccord.getNum()).isEqualTo(UPDATED_NUM);
        assertThat(testDemandeAccord.getTitre()).isEqualTo(UPDATED_TITRE);
        assertThat(testDemandeAccord.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testDemandeAccord.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDemandeAccord.getTexteAccord()).isEqualTo(UPDATED_TEXTE_ACCORD);
        assertThat(testDemandeAccord.getDateDemande()).isEqualTo(UPDATED_DATE_DEMANDE);
        assertThat(testDemandeAccord.isValidation()).isEqualTo(UPDATED_VALIDATION);
    }

    @Test
    @Transactional
    public void updateNonExistingDemandeAccord() throws Exception {
        int databaseSizeBeforeUpdate = demandeAccordRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDemandeAccordMockMvc.perform(put("/api/demande-accords")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(demandeAccord)))
            .andExpect(status().isBadRequest());

        // Validate the DemandeAccord in the database
        List<DemandeAccord> demandeAccordList = demandeAccordRepository.findAll();
        assertThat(demandeAccordList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDemandeAccord() throws Exception {
        // Initialize the database
        demandeAccordRepository.saveAndFlush(demandeAccord);

        int databaseSizeBeforeDelete = demandeAccordRepository.findAll().size();

        // Delete the demandeAccord
        restDemandeAccordMockMvc.perform(delete("/api/demande-accords/{id}", demandeAccord.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DemandeAccord> demandeAccordList = demandeAccordRepository.findAll();
        assertThat(demandeAccordList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
