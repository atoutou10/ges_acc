package sn.uvs.il.web.rest;

import sn.uvs.il.GesAccApp;
import sn.uvs.il.domain.Avenant;
import sn.uvs.il.repository.AvenantRepository;
import sn.uvs.il.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static sn.uvs.il.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AvenantResource} REST controller.
 */
@SpringBootTest(classes = GesAccApp.class)
//<<<<<<< HEAD
//=======
@AutoConfigureMockMvc
@WithMockUser
//>>>>>>> jhipster_upgrade
public class AvenantResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final String DEFAULT_DOCUMENT = "AAAAAAAAAA";
    private static final String UPDATED_DOCUMENT = "BBBBBBBBBB";

    private static final String DEFAULT_BUT = "AAAAAAAAAA";
    private static final String UPDATED_BUT = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_SIGNATURE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_SIGNATURE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_EXPIRATION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_EXPIRATION = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    @Autowired
    private AvenantRepository avenantRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAvenantMockMvc;

    private Avenant avenant;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AvenantResource avenantResource = new AvenantResource(avenantRepository);
        this.restAvenantMockMvc = MockMvcBuilders.standaloneSetup(avenantResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Avenant createEntity(EntityManager em) {
        Avenant avenant = new Avenant()
            .libelle(DEFAULT_LIBELLE)
            .document(DEFAULT_DOCUMENT)
            .but(DEFAULT_BUT)
            .description(DEFAULT_DESCRIPTION)
            .dateSignature(DEFAULT_DATE_SIGNATURE)
            .dateExpiration(DEFAULT_DATE_EXPIRATION)
            .status(DEFAULT_STATUS);
        return avenant;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Avenant createUpdatedEntity(EntityManager em) {
        Avenant avenant = new Avenant()
            .libelle(UPDATED_LIBELLE)
            .document(UPDATED_DOCUMENT)
            .but(UPDATED_BUT)
            .description(UPDATED_DESCRIPTION)
            .dateSignature(UPDATED_DATE_SIGNATURE)
            .dateExpiration(UPDATED_DATE_EXPIRATION)
            .status(UPDATED_STATUS);
        return avenant;
    }

    @BeforeEach
    public void initTest() {
        avenant = createEntity(em);
    }

    @Test
    @Transactional
    public void createAvenant() throws Exception {
        int databaseSizeBeforeCreate = avenantRepository.findAll().size();
        // Create the Avenant
        restAvenantMockMvc.perform(post("/api/avenants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(avenant)))
            .andExpect(status().isCreated());

        // Validate the Avenant in the database
        List<Avenant> avenantList = avenantRepository.findAll();
        assertThat(avenantList).hasSize(databaseSizeBeforeCreate + 1);
        Avenant testAvenant = avenantList.get(avenantList.size() - 1);
        assertThat(testAvenant.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testAvenant.getDocument()).isEqualTo(DEFAULT_DOCUMENT);
        assertThat(testAvenant.getBut()).isEqualTo(DEFAULT_BUT);
        assertThat(testAvenant.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testAvenant.getDateSignature()).isEqualTo(DEFAULT_DATE_SIGNATURE);
        assertThat(testAvenant.getDateExpiration()).isEqualTo(DEFAULT_DATE_EXPIRATION);
        assertThat(testAvenant.isStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createAvenantWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = avenantRepository.findAll().size();

        // Create the Avenant with an existing ID
        avenant.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAvenantMockMvc.perform(post("/api/avenants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(avenant)))
            .andExpect(status().isBadRequest());

        // Validate the Avenant in the database
        List<Avenant> avenantList = avenantRepository.findAll();
        assertThat(avenantList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAvenants() throws Exception {
        // Initialize the database
        avenantRepository.saveAndFlush(avenant);

        // Get all the avenantList
        restAvenantMockMvc.perform(get("/api/avenants?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(avenant.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].document").value(hasItem(DEFAULT_DOCUMENT)))
            .andExpect(jsonPath("$.[*].but").value(hasItem(DEFAULT_BUT)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].dateSignature").value(hasItem(DEFAULT_DATE_SIGNATURE.toString())))
            .andExpect(jsonPath("$.[*].dateExpiration").value(hasItem(DEFAULT_DATE_EXPIRATION.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getAvenant() throws Exception {
        // Initialize the database
        avenantRepository.saveAndFlush(avenant);

        // Get the avenant
        restAvenantMockMvc.perform(get("/api/avenants/{id}", avenant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(avenant.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.document").value(DEFAULT_DOCUMENT))
            .andExpect(jsonPath("$.but").value(DEFAULT_BUT))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.dateSignature").value(DEFAULT_DATE_SIGNATURE.toString()))
            .andExpect(jsonPath("$.dateExpiration").value(DEFAULT_DATE_EXPIRATION.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingAvenant() throws Exception {
        // Get the avenant
        restAvenantMockMvc.perform(get("/api/avenants/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAvenant() throws Exception {
        // Initialize the database
        avenantRepository.saveAndFlush(avenant);

        int databaseSizeBeforeUpdate = avenantRepository.findAll().size();

        // Update the avenant
        Avenant updatedAvenant = avenantRepository.findById(avenant.getId()).get();
        // Disconnect from session so that the updates on updatedAvenant are not directly saved in db
        em.detach(updatedAvenant);
        updatedAvenant
            .libelle(UPDATED_LIBELLE)
            .document(UPDATED_DOCUMENT)
            .but(UPDATED_BUT)
            .description(UPDATED_DESCRIPTION)
            .dateSignature(UPDATED_DATE_SIGNATURE)
            .dateExpiration(UPDATED_DATE_EXPIRATION)
            .status(UPDATED_STATUS);

        restAvenantMockMvc.perform(put("/api/avenants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAvenant)))
            .andExpect(status().isOk());

        // Validate the Avenant in the database
        List<Avenant> avenantList = avenantRepository.findAll();
        assertThat(avenantList).hasSize(databaseSizeBeforeUpdate);
        Avenant testAvenant = avenantList.get(avenantList.size() - 1);
        assertThat(testAvenant.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testAvenant.getDocument()).isEqualTo(UPDATED_DOCUMENT);
        assertThat(testAvenant.getBut()).isEqualTo(UPDATED_BUT);
        assertThat(testAvenant.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testAvenant.getDateSignature()).isEqualTo(UPDATED_DATE_SIGNATURE);
        assertThat(testAvenant.getDateExpiration()).isEqualTo(UPDATED_DATE_EXPIRATION);
        assertThat(testAvenant.isStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingAvenant() throws Exception {
        int databaseSizeBeforeUpdate = avenantRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAvenantMockMvc.perform(put("/api/avenants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(avenant)))
            .andExpect(status().isBadRequest());

        // Validate the Avenant in the database
        List<Avenant> avenantList = avenantRepository.findAll();
        assertThat(avenantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAvenant() throws Exception {
        // Initialize the database
        avenantRepository.saveAndFlush(avenant);

        int databaseSizeBeforeDelete = avenantRepository.findAll().size();

        // Delete the avenant
        restAvenantMockMvc.perform(delete("/api/avenants/{id}", avenant.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Avenant> avenantList = avenantRepository.findAll();
        assertThat(avenantList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
