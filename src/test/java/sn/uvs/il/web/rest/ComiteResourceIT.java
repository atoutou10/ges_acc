package sn.uvs.il.web.rest;

import sn.uvs.il.GesAccApp;
import sn.uvs.il.domain.Comite;
import sn.uvs.il.repository.ComiteRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ComiteResource} REST controller.
 */
@SpringBootTest(classes = GesAccApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ComiteResourceIT {

    private static final String DEFAULT_ROLE = "AAAAAAAAAA";
    private static final String UPDATED_ROLE = "BBBBBBBBBB";

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    @Autowired
    private ComiteRepository comiteRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restComiteMockMvc;

    private Comite comite;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Comite createEntity(EntityManager em) {
        Comite comite = new Comite()
            .role(DEFAULT_ROLE)
            .nom(DEFAULT_NOM);
        return comite;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Comite createUpdatedEntity(EntityManager em) {
        Comite comite = new Comite()
            .role(UPDATED_ROLE)
            .nom(UPDATED_NOM);
        return comite;
    }

    @BeforeEach
    public void initTest() {
        comite = createEntity(em);
    }

    @Test
    @Transactional
    public void createComite() throws Exception {
        int databaseSizeBeforeCreate = comiteRepository.findAll().size();
        // Create the Comite
        restComiteMockMvc.perform(post("/api/comites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(comite)))
            .andExpect(status().isCreated());

        // Validate the Comite in the database
        List<Comite> comiteList = comiteRepository.findAll();
        assertThat(comiteList).hasSize(databaseSizeBeforeCreate + 1);
        Comite testComite = comiteList.get(comiteList.size() - 1);
        assertThat(testComite.getRole()).isEqualTo(DEFAULT_ROLE);
        assertThat(testComite.getNom()).isEqualTo(DEFAULT_NOM);
    }

    @Test
    @Transactional
    public void createComiteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = comiteRepository.findAll().size();

        // Create the Comite with an existing ID
        comite.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restComiteMockMvc.perform(post("/api/comites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(comite)))
            .andExpect(status().isBadRequest());

        // Validate the Comite in the database
        List<Comite> comiteList = comiteRepository.findAll();
        assertThat(comiteList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllComites() throws Exception {
        // Initialize the database
        comiteRepository.saveAndFlush(comite);

        // Get all the comiteList
        restComiteMockMvc.perform(get("/api/comites?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(comite.getId().intValue())))
            .andExpect(jsonPath("$.[*].role").value(hasItem(DEFAULT_ROLE)))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)));
    }
    
    @Test
    @Transactional
    public void getComite() throws Exception {
        // Initialize the database
        comiteRepository.saveAndFlush(comite);

        // Get the comite
        restComiteMockMvc.perform(get("/api/comites/{id}", comite.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(comite.getId().intValue()))
            .andExpect(jsonPath("$.role").value(DEFAULT_ROLE))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM));
    }
    @Test
    @Transactional
    public void getNonExistingComite() throws Exception {
        // Get the comite
        restComiteMockMvc.perform(get("/api/comites/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateComite() throws Exception {
        // Initialize the database
        comiteRepository.saveAndFlush(comite);

        int databaseSizeBeforeUpdate = comiteRepository.findAll().size();

        // Update the comite
        Comite updatedComite = comiteRepository.findById(comite.getId()).get();
        // Disconnect from session so that the updates on updatedComite are not directly saved in db
        em.detach(updatedComite);
        updatedComite
            .role(UPDATED_ROLE)
            .nom(UPDATED_NOM);

        restComiteMockMvc.perform(put("/api/comites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedComite)))
            .andExpect(status().isOk());

        // Validate the Comite in the database
        List<Comite> comiteList = comiteRepository.findAll();
        assertThat(comiteList).hasSize(databaseSizeBeforeUpdate);
        Comite testComite = comiteList.get(comiteList.size() - 1);
        assertThat(testComite.getRole()).isEqualTo(UPDATED_ROLE);
        assertThat(testComite.getNom()).isEqualTo(UPDATED_NOM);
    }

    @Test
    @Transactional
    public void updateNonExistingComite() throws Exception {
        int databaseSizeBeforeUpdate = comiteRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restComiteMockMvc.perform(put("/api/comites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(comite)))
            .andExpect(status().isBadRequest());

        // Validate the Comite in the database
        List<Comite> comiteList = comiteRepository.findAll();
        assertThat(comiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteComite() throws Exception {
        // Initialize the database
        comiteRepository.saveAndFlush(comite);

        int databaseSizeBeforeDelete = comiteRepository.findAll().size();

        // Delete the comite
        restComiteMockMvc.perform(delete("/api/comites/{id}", comite.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Comite> comiteList = comiteRepository.findAll();
        assertThat(comiteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
