package sn.uvs.il.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.uvs.il.web.rest.TestUtil;

public class TypePartenaireTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TypePartenaire.class);
        TypePartenaire typePartenaire1 = new TypePartenaire();
        typePartenaire1.setId(1L);
        TypePartenaire typePartenaire2 = new TypePartenaire();
        typePartenaire2.setId(typePartenaire1.getId());
        assertThat(typePartenaire1).isEqualTo(typePartenaire2);
        typePartenaire2.setId(2L);
        assertThat(typePartenaire1).isNotEqualTo(typePartenaire2);
        typePartenaire1.setId(null);
        assertThat(typePartenaire1).isNotEqualTo(typePartenaire2);
    }
}
