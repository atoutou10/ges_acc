package sn.uvs.il.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.uvs.il.web.rest.TestUtil;

public class DemandeAccordTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DemandeAccord.class);
        DemandeAccord demandeAccord1 = new DemandeAccord();
        demandeAccord1.setId(1L);
        DemandeAccord demandeAccord2 = new DemandeAccord();
        demandeAccord2.setId(demandeAccord1.getId());
        assertThat(demandeAccord1).isEqualTo(demandeAccord2);
        demandeAccord2.setId(2L);
        assertThat(demandeAccord1).isNotEqualTo(demandeAccord2);
        demandeAccord1.setId(null);
        assertThat(demandeAccord1).isNotEqualTo(demandeAccord2);
    }
}
