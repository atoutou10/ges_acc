package sn.uvs.il.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.uvs.il.web.rest.TestUtil;

public class ComiteTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Comite.class);
        Comite comite1 = new Comite();
        comite1.setId(1L);
        Comite comite2 = new Comite();
        comite2.setId(comite1.getId());
        assertThat(comite1).isEqualTo(comite2);
        comite2.setId(2L);
        assertThat(comite1).isNotEqualTo(comite2);
        comite1.setId(null);
        assertThat(comite1).isNotEqualTo(comite2);
    }
}
