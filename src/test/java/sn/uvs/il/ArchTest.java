package sn.uvs.il;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("sn.uvs.il");

        noClasses()
            .that()
                .resideInAnyPackage("sn.uvs.il.service..")
            .or()
                .resideInAnyPackage("sn.uvs.il.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..sn.uvs.il.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
