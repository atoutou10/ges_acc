import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GesAccTestModule } from '../../../test.module';
import { ComiteDetailComponent } from 'app/entities/comite/comite-detail.component';
import { Comite } from 'app/shared/model/comite.model';

describe('Component Tests', () => {
  describe('Comite Management Detail Component', () => {
    let comp: ComiteDetailComponent;
    let fixture: ComponentFixture<ComiteDetailComponent>;
    const route = ({ data: of({ comite: new Comite(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GesAccTestModule],
        declarations: [ComiteDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ComiteDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ComiteDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load comite on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.comite).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
