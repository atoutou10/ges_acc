import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GesAccTestModule } from '../../../test.module';
import { ComiteComponent } from 'app/entities/comite/comite.component';
import { ComiteService } from 'app/entities/comite/comite.service';
import { Comite } from 'app/shared/model/comite.model';

describe('Component Tests', () => {
  describe('Comite Management Component', () => {
    let comp: ComiteComponent;
    let fixture: ComponentFixture<ComiteComponent>;
    let service: ComiteService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GesAccTestModule],
        declarations: [ComiteComponent],
      })
        .overrideTemplate(ComiteComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ComiteComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ComiteService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Comite(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.comites && comp.comites[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
