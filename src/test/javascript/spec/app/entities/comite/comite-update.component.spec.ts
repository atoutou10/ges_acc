import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GesAccTestModule } from '../../../test.module';
import { ComiteUpdateComponent } from 'app/entities/comite/comite-update.component';
import { ComiteService } from 'app/entities/comite/comite.service';
import { Comite } from 'app/shared/model/comite.model';

describe('Component Tests', () => {
  describe('Comite Management Update Component', () => {
    let comp: ComiteUpdateComponent;
    let fixture: ComponentFixture<ComiteUpdateComponent>;
    let service: ComiteService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GesAccTestModule],
        declarations: [ComiteUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ComiteUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ComiteUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ComiteService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Comite(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Comite();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
