import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GesAccTestModule } from '../../../test.module';
import { AvenantDetailComponent } from 'app/entities/avenant/avenant-detail.component';
import { Avenant } from 'app/shared/model/avenant.model';

describe('Component Tests', () => {
  describe('Avenant Management Detail Component', () => {
    let comp: AvenantDetailComponent;
    let fixture: ComponentFixture<AvenantDetailComponent>;
    const route = ({ data: of({ avenant: new Avenant(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GesAccTestModule],
        declarations: [AvenantDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(AvenantDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AvenantDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load avenant on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.avenant).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
