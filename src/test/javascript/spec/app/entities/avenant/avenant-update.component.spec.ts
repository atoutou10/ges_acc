import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GesAccTestModule } from '../../../test.module';
import { AvenantUpdateComponent } from 'app/entities/avenant/avenant-update.component';
import { AvenantService } from 'app/entities/avenant/avenant.service';
import { Avenant } from 'app/shared/model/avenant.model';

describe('Component Tests', () => {
  describe('Avenant Management Update Component', () => {
    let comp: AvenantUpdateComponent;
    let fixture: ComponentFixture<AvenantUpdateComponent>;
    let service: AvenantService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GesAccTestModule],
        declarations: [AvenantUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(AvenantUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AvenantUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AvenantService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Avenant(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Avenant();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
