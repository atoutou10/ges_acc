import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GesAccTestModule } from '../../../test.module';
import { AvenantComponent } from 'app/entities/avenant/avenant.component';
import { AvenantService } from 'app/entities/avenant/avenant.service';
import { Avenant } from 'app/shared/model/avenant.model';

describe('Component Tests', () => {
  describe('Avenant Management Component', () => {
    let comp: AvenantComponent;
    let fixture: ComponentFixture<AvenantComponent>;
    let service: AvenantService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GesAccTestModule],
        declarations: [AvenantComponent],
        providers: [],
      })
        .overrideTemplate(AvenantComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AvenantComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AvenantService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Avenant(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.avenants && comp.avenants[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
