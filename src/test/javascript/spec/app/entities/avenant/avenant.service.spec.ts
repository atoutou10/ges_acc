import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { AvenantService } from 'app/entities/avenant/avenant.service';
import { IAvenant, Avenant } from 'app/shared/model/avenant.model';

describe('Service Tests', () => {
  describe('Avenant Service', () => {
    let injector: TestBed;
    let service: AvenantService;
    let httpMock: HttpTestingController;
    let elemDefault: IAvenant;
    let expectedResult: IAvenant | IAvenant[] | boolean | null;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(AvenantService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Avenant(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', currentDate, currentDate, false);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateSignature: currentDate.format(DATE_FORMAT),
            dateExpiration: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Avenant', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateSignature: currentDate.format(DATE_FORMAT),
            dateExpiration: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateSignature: currentDate,
            dateExpiration: currentDate,
          },
          returnedFromService
        );
        service
          .create(new Avenant())
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Avenant', () => {
        const returnedFromService = Object.assign(
          {
            libelle: 'BBBBBB',
            document: 'BBBBBB',
            but: 'BBBBBB',
            description: 'BBBBBB',
            dateSignature: currentDate.format(DATE_FORMAT),
            dateExpiration: currentDate.format(DATE_FORMAT),
            status: true,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateSignature: currentDate,
            dateExpiration: currentDate,
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Avenant', () => {
        const returnedFromService = Object.assign(
          {
            libelle: 'BBBBBB',
            document: 'BBBBBB',
            but: 'BBBBBB',
            description: 'BBBBBB',
            dateSignature: currentDate.format(DATE_FORMAT),
            dateExpiration: currentDate.format(DATE_FORMAT),
            status: true,
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateSignature: currentDate,
            dateExpiration: currentDate,
          },
          returnedFromService
        );
        service
          .query()
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Avenant', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
