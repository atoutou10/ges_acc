import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GesAccTestModule } from '../../../test.module';
import { DemandeAccordComponent } from 'app/entities/demande-accord/demande-accord.component';
import { DemandeAccordService } from 'app/entities/demande-accord/demande-accord.service';
import { DemandeAccord } from 'app/shared/model/demande-accord.model';

describe('Component Tests', () => {
  describe('DemandeAccord Management Component', () => {
    let comp: DemandeAccordComponent;
    let fixture: ComponentFixture<DemandeAccordComponent>;
    let service: DemandeAccordService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GesAccTestModule],
        declarations: [DemandeAccordComponent],
        providers: [],
      })
        .overrideTemplate(DemandeAccordComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DemandeAccordComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DemandeAccordService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new DemandeAccord(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.demandeAccords && comp.demandeAccords[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
