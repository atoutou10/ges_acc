import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GesAccTestModule } from '../../../test.module';
import { DemandeAccordDetailComponent } from 'app/entities/demande-accord/demande-accord-detail.component';
import { DemandeAccord } from 'app/shared/model/demande-accord.model';

describe('Component Tests', () => {
  describe('DemandeAccord Management Detail Component', () => {
    let comp: DemandeAccordDetailComponent;
    let fixture: ComponentFixture<DemandeAccordDetailComponent>;
    const route = ({ data: of({ demandeAccord: new DemandeAccord(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GesAccTestModule],
        declarations: [DemandeAccordDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(DemandeAccordDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DemandeAccordDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load demandeAccord on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.demandeAccord).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
