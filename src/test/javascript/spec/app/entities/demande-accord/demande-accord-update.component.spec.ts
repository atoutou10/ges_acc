import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GesAccTestModule } from '../../../test.module';
import { DemandeAccordUpdateComponent } from 'app/entities/demande-accord/demande-accord-update.component';
import { DemandeAccordService } from 'app/entities/demande-accord/demande-accord.service';
import { DemandeAccord } from 'app/shared/model/demande-accord.model';

describe('Component Tests', () => {
  describe('DemandeAccord Management Update Component', () => {
    let comp: DemandeAccordUpdateComponent;
    let fixture: ComponentFixture<DemandeAccordUpdateComponent>;
    let service: DemandeAccordService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GesAccTestModule],
        declarations: [DemandeAccordUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(DemandeAccordUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DemandeAccordUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DemandeAccordService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new DemandeAccord(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new DemandeAccord();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
