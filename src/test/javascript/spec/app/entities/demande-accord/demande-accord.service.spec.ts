import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { DemandeAccordService } from 'app/entities/demande-accord/demande-accord.service';
import { IDemandeAccord, DemandeAccord } from 'app/shared/model/demande-accord.model';

describe('Service Tests', () => {
  describe('DemandeAccord Service', () => {
    let injector: TestBed;
    let service: DemandeAccordService;
    let httpMock: HttpTestingController;
    let elemDefault: IDemandeAccord;
    let expectedResult: IDemandeAccord | IDemandeAccord[] | boolean | null;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(DemandeAccordService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new DemandeAccord(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', currentDate, false);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateDemande: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a DemandeAccord', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateDemande: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateDemande: currentDate,
          },
          returnedFromService
        );
        service
          .create(new DemandeAccord())
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a DemandeAccord', () => {
        const returnedFromService = Object.assign(
          {
            num: 'BBBBBB',
            titre: 'BBBBBB',
            type: 'BBBBBB',
            description: 'BBBBBB',
            texteAccord: 'BBBBBB',
            dateDemande: currentDate.format(DATE_FORMAT),
            validation: true,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateDemande: currentDate,
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of DemandeAccord', () => {
        const returnedFromService = Object.assign(
          {
            num: 'BBBBBB',
            titre: 'BBBBBB',
            type: 'BBBBBB',
            description: 'BBBBBB',
            texteAccord: 'BBBBBB',
            dateDemande: currentDate.format(DATE_FORMAT),
            validation: true,
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateDemande: currentDate,
          },
          returnedFromService
        );
        service
          .query()
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DemandeAccord', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
