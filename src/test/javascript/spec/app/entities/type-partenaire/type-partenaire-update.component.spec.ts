import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GesAccTestModule } from '../../../test.module';
import { TypePartenaireUpdateComponent } from 'app/entities/type-partenaire/type-partenaire-update.component';
import { TypePartenaireService } from 'app/entities/type-partenaire/type-partenaire.service';
import { TypePartenaire } from 'app/shared/model/type-partenaire.model';

describe('Component Tests', () => {
  describe('TypePartenaire Management Update Component', () => {
    let comp: TypePartenaireUpdateComponent;
    let fixture: ComponentFixture<TypePartenaireUpdateComponent>;
    let service: TypePartenaireService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GesAccTestModule],
        declarations: [TypePartenaireUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(TypePartenaireUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TypePartenaireUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TypePartenaireService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TypePartenaire(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TypePartenaire();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
