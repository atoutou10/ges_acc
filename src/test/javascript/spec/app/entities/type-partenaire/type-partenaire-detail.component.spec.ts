import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GesAccTestModule } from '../../../test.module';
import { TypePartenaireDetailComponent } from 'app/entities/type-partenaire/type-partenaire-detail.component';
import { TypePartenaire } from 'app/shared/model/type-partenaire.model';

describe('Component Tests', () => {
  describe('TypePartenaire Management Detail Component', () => {
    let comp: TypePartenaireDetailComponent;
    let fixture: ComponentFixture<TypePartenaireDetailComponent>;
    const route = ({ data: of({ typePartenaire: new TypePartenaire(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GesAccTestModule],
        declarations: [TypePartenaireDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(TypePartenaireDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TypePartenaireDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load typePartenaire on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.typePartenaire).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
