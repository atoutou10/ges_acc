import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GesAccTestModule } from '../../../test.module';
import { TypePartenaireComponent } from 'app/entities/type-partenaire/type-partenaire.component';
import { TypePartenaireService } from 'app/entities/type-partenaire/type-partenaire.service';
import { TypePartenaire } from 'app/shared/model/type-partenaire.model';

describe('Component Tests', () => {
  describe('TypePartenaire Management Component', () => {
    let comp: TypePartenaireComponent;
    let fixture: ComponentFixture<TypePartenaireComponent>;
    let service: TypePartenaireService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GesAccTestModule],
        declarations: [TypePartenaireComponent],
      })
        .overrideTemplate(TypePartenaireComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TypePartenaireComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TypePartenaireService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new TypePartenaire(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.typePartenaires && comp.typePartenaires[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
