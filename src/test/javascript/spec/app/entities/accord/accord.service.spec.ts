import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { AccordService } from 'app/entities/accord/accord.service';
import { IAccord, Accord } from 'app/shared/model/accord.model';

describe('Service Tests', () => {
  describe('Accord Service', () => {
    let injector: TestBed;
    let service: AccordService;
    let httpMock: HttpTestingController;
    let elemDefault: IAccord;
    let expectedResult: IAccord | IAccord[] | boolean | null;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(AccordService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Accord(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', currentDate, currentDate, false);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateSignature: currentDate.format(DATE_FORMAT),
            dateExpiration: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Accord', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateSignature: currentDate.format(DATE_FORMAT),
            dateExpiration: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateSignature: currentDate,
            dateExpiration: currentDate,
          },
          returnedFromService
        );
        service
          .create(new Accord())
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Accord', () => {
        const returnedFromService = Object.assign(
          {
            nom: 'BBBBBB',
            titre: 'BBBBBB',
            domaine: 'BBBBBB',
            description: 'BBBBBB',
            but: 'BBBBBB',
            dateSignature: currentDate.format(DATE_FORMAT),
            dateExpiration: currentDate.format(DATE_FORMAT),
            status: true,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateSignature: currentDate,
            dateExpiration: currentDate,
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Accord', () => {
        const returnedFromService = Object.assign(
          {
            nom: 'BBBBBB',
            titre: 'BBBBBB',
            domaine: 'BBBBBB',
            description: 'BBBBBB',
            but: 'BBBBBB',
            dateSignature: currentDate.format(DATE_FORMAT),
            dateExpiration: currentDate.format(DATE_FORMAT),
            status: true,
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateSignature: currentDate,
            dateExpiration: currentDate,
          },
          returnedFromService
        );
        service
          .query()
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Accord', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
